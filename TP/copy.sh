#!/bin/zsh
cp -f html/exercise1.html $1/html
cp -f html/htmlLab.html $1/html
cp -f js/jsLab.html $1/js
cp -f js2/js2Lab.html $1/js2
cp -f js2/m.txt $1/js2
cp -f server/serverLab.html $1/server
cp -f server2/server2Lab.html $1/server2
cp -f server2/db.json $1/server2
cp -f webapp/webappLab.html $1/webapp
cp -f webapp/minimalfolderstruct.png $1/webapp
cp -f webapp/exemple.png $1/webapp
