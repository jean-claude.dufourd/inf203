# Lab: Web Application

<div class="activity">

The purpose of this lab is to study the concepts discussed during [INF203
on Web server technologies](http://perso.telecom-paris.fr/dufourd/cours/inf203/).

Rather than installing a turnkey web server (such as Apache),
set up (e.g. modify httpd.conf configuration files) and
to add specific behaviors in languages like PHP,
Java, C / C ++ ..., this lab proposes to create a server from scratch (or
almost). You will program in JavaScript, using NodeJS and
its support for HTTP / HTTPS protocols.
</div>

## To do

<div class="activity">

For this lab, you will need to upload the different JavaScript files
you created. Everything will have to be zipped.

To program, use the "strict" mode of JavaScript, the indentation and
comments. 

***Thank you for using zip (and not tar and gzip or bzip2). Zip all
files directly into a single zip, do not zip the folder.***

***No spaces or accented characters or special characters in
the name of the zip file.***

***This lab is to be done alone***.

Please respect the file names and IDs we ask you
to use, otherwise automatic grading will not work and
you will not have the grade matching your work.

**Use local URLs in your files**, so your production
works regardless of the URL of the server, and so that it
also works on the grading machine.

In all your functions that respond to HTTP requests, put a `try
{} catch {}` to catch all the exceptions, and show the
error messages.

To debug any problem, please make sure you have opened the JavaScript console of your browser and looked for error messages.

In Chrome, "Inspect" in the context menu (right click), option-cmd-C on a Mac, ctrl-shift-I on Windows.

In Firefox, "Inspect Element" in the context menu (right click), option-cmd-C on a Mac, ctrl-shift-C on Windows.

In Safari, "Inspect Element" in the context menu (right click), option-cmd-C on a Mac.

In Edge, "Inspect Element" in the context menu (right click), F12 Developer Toole on a Windows PC. 

</div>

## Use of NodeJS modules

<div class="activity">

NodeJS is based on the concept of **modules**. A module is a block of
JavaScript code that you can load and use in your code,
that is to say as a library. To load a module, NodeJS
uses the `require` function. It is used like this:

```javascript
var module = require ('module_name');
```

There are many modules available for NodeJS. Some are
installed automatically with NodeJS (see the documentation
[Here](https://nodejs.org/api/)){target="_blank"}. In this case, you just have to use
`require` to use the module. This is the case for the `fs` module of
management of the "File System", used to read / write files
(see the documentation of this module
[here](https://nodejs.org/api/fs.html)){target="_blank"}, or even for the module
`http` which allows to receive and send HTTP messages (see the
documentation of this module [here](https://nodejs.org/api/http.html)){target="_blank"}.

NodeJS provides the command line tool `npm` to load more modules. 
To download and install a module,
use the following command line:

```bash
> npm init //only do this line the first time
> npm install module_name --save
```

`npm init` creates a package.json. You can use the default
response to most questions.

The `--save` option adds a dependency to the `package.json` file which is required 
for the grader to know that it needs to load this extra module.

If you have a problem with `npm`, for example it does not
exist on your system, look [there for instructions](#npm){target="_blank"}

You can verify that a module is installed by checking that 
there is a subdirectory with the module name in the 
`node_modules` folder. You can then use `require`. 

For example,
to install the `http` module which is a simple http server,
use:

```bash
> npm install http
```

And then, in the JavaScript code, write:

``` 
var http = require('http');
```

</div>

## Exercise - A pseudo data base

<div class="activity">

Use a server similar to the one you developed in the Simple Server Lab to create / edit a database
and then to display the contents of the database. 
The pseudo database will be a text file containing
JSON data. Put the client in a `client` folder. The JSON file will be
`storage.json`. 
The `storage.json` file will have the structure:` [{"title": "foo", "color":
"red", "value": 20}, {"title": "bar", "color": "ivory", "value": 100}, ...] `

Values are strictly positive numbers. Add values to a total that will represent
100% and 360° of the pie. Display percentages on each wedge.

**Question 0:** Take the code of the simple server lab and remove all but:

- answer something to URL "/" to help you get that the server works, possibly with a name and version number
- serve files from the current execution folder when the URL starts with "/°°va/"
- exit the server when receiving the URL "/°°vb", this just to help me with keeping the grading server simple. Thank you.

**Questions 1 to 5:** Client-side: create a button bar with "show txt", "add
element", "remove element", "clear".
The actions of these buttons are:

1. `show txt`: shows the text of the current JSON
1. `add element`: adds in the current JSON an
element whose information is a number (value), a text
(title) and a color in CSS format (color name or hex)
(color). The new element is at the end of the list.
1. `remove element`: deletes an element in the
running with his index finger
1. `clear`: deletes all elements in the current JSON, and replaces
them with one constant element named empty, value 1 and color
 red.
1. `restore`: restores a `storage.json` file with 3 slices, this last action
is intended to simplify the (semi) automatic grading.

Put the buttons at the top of the space as a menu bar and
the display space below. Each button clears
the display space and shows what is necessary. 
For each button, there is something
to do in the editing page and something to do in the
server.
 
In order to allow auto-grading, please put ids on the buttons in the toolbar:
 
* "°°vc" for the button to show the text of the current JSON below the menu bar, 
* "°°vd" for the button to show the form to add a new element to the JSON,
* "REMOVE" for the button to show the form to remove an existing element from the JSON,
* "CLEAR" for the button to clear the existing JSON,
* "°°vg" for the button to show a piechart computed on the server,
* "°°vh" for the button to show a piechart computed on the client.

You also need textfields for the index to remove and the title, color and value to add, 
use "indexTF", "titleTF", "colorTF" and "valueTF" for them.
The buttons to validate addition and removal shall have "°°vi" and "°°vj" as ids.
The element in which you insert the result of show shall have the id "MAINSHOW".

Server: Make sure that the JSON on the server is changed. URLs
querying the server will be:

1. `http://localhost:8000/°°vk`
1. `http://localhost:8000/add?title=*&value=**&color=***`
where * is a string, ** a number and *** a CSS color
1. `http://localhost:8000/remove?index=*` where * is a number
1. `http://localhost:8000/clear`
1. `http://localhost:8000/°°va/...`

File name: `client/test2.html`,`client/test2.js`, `server1.mjs`

<div class="attention">
WARNING: do not put URLs with `http://localhost:8000/` in test2.html
and test2.js. When deployed and used over another computer than
localhost, these URLs will stop working. In the files, use 
`relative URLs`, e.g. `../../show` rather than 
`http://localhost:8000/show`

The labgrader will deploy your code on other machines, so your grade
 depends on heeding this warning.

Why `../../°°vk` rather than just `/°°vk` ? In your local server, 
 `/°°vk` will work, but not in the context of the grader. 

 The grader uses
 HTTPS for authentication, and browsers forbid the mixing of 
 HTTP and HTTPS.

Because you use HTTPS to connect to the grader,
 the grader cannot request `http://localhost:8000/°°vk`; the grader
 requests `https://labgrader.telecom-paris.fr/redir/8000/°°vk` to 
 achieve the same.

 Since your test2.html file is served from
 `https://labgrader.telecom-paris.fr/redir/8000/files/client/test2.html`
 then `../../°°vk` will be right both for your local server and
 the grader context.
</div>

-----

**Question 6:** Client-side: add the `show piechart` button in `test2.html`. 
In this exercise you will create the SVG
on the server and then display it in an image in the HTML on the client. The media type of SVG
is "image/svg+xml". The SVG namespace is "http://www.w3.org/2000/svg".
To make pie wedges, use path objects,
the fill attribute for the color, the d field for the plot, and in the
plot use M to move, L to make a line, A to create an arc and Z to close the path.

Per wedge, an SVG text displays the field "title". The text appears
on the colorful area of ​​the wedge.

You will add the processing of URL `http://localhost:8000/°°vl` in the server.

Reuse the same files as for question 2a.

Suggestion: start by writing an SVG file that draws a
pie, then use it as a model.

Find a way to adjust the color of the text so that the text is
always visible, regardless of the color choice of the wedges.

Here is how it could look (without the title text and percentage per wedge):

![](exemple.png)

-----

**Question 7:** In this question, you will see one of the choices of
any web developer: do the work on the server as you have
done in question 2b or do the work in the browser. Add
a "show local piechart" button that will use the relative URL
`/°°vk` to get the JSON, then create the SVG
directly into the local browser and then insert it into the HTML
to display it.

</div>

## Automatic grading

<div class="activity">

To grade your TP, you can just drop your zip below.

The zip file MUST be called `tpwebapp.zip`

>***How to get help on grading messages***
>
>If anything goes wrong in the process of grading your work, contact [me](mailto:dufourd@telecom-paris.fr){target="_blank"}.
>This grader keeps the last upload you have made for each lab.
>If you have a problem, I can instruct the grader to replay any of your labs, or take a look at your uploaded code.
>So you do not need to send me your JS code by email, which is forbidden by email security rules at Telecom.
>Just ***tell me which lab you are working on and which error message from the grader is blocking***.

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"><img src="https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png"/></a>

</div>

<script type="application/javascript">var currentGradingLab = "webapp";</script>
<iframe src="/images/dragdrop.html" style="width: 100%; height: 500px;"/>
