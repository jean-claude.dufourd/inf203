# Server Lab for Design911

This is a lab to introduce to practical aspects of a web server. 

# Minimal server

<div class = "activity">

Here is a minimal server in node.js: let's look at its parts

```{.javascript .numberLines}
"use strict";

import {createServer} from "http";
import {readFileSync, existsSync} from "fs";
import pkg from "mime";
const {getType} = pkg;

function webserver( request, response ) {
  const filepath = request.url.substring(1); // remove initial /
  if (existsSync(filepath)) {
    response.setHeader("Content-Type", getType(filepath));
    response.end(readFileSync(filepath));
  } else {
    response.statusCode = 404;
    response.end("the file " + filepath + " does not exist on the server");
  }
}

const server = createServer(webserver);

server.listen(process.argv[2], (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("server started on port "+process.argv[2]);
  }
});
``` 

Line 1 is required to tell the JavaScript interpreter to use the secure options.

Lines 3-6 are the imports of the modules we are using in this code. mime is an old module with a bizarre syntax.

Lines 8-17 are the code that deals with serving files:

* on line 9, the URL in the request is transformed into a file path, in this case by removing the initial /
* on line 10, the code tests if there is a file corresponding to the URL
* on line 11 the Content-Type header for the response is set
* on line 12 or 15 the response is sent (actually, the sending is started)

Line 19 tells the server: when you receive a request, use function `webserver` to process it.

Lines 21-27 start the server

Put this code in a file called `test.mjs`. mjs is the extensions for ES6 or EcmaScript 6, the latest version of
JavaScript available (almost) everywhere.
Place this file in an empty folder. 

Then you need to import the modules that this code uses with:

```shell
npm install --save http
npm install --save fs
npm install --save mime
```
 
The http module contains a basic http server. The fs module contains all the file I/O code.
The mime module contains code that guesses the Content-Type of a file. These modules are placed
in a subfolder called node_modules

Run this server with `node test.mjs 3000` and test the server on localhost:3000 from 
your browser.

Try different URLs, for files that exist and files that do not.
Use the command `curl -i <URL>` to see the response headers.

</div>

# Exercise 1

<div class = "activity">

In this exercise, you will modify the server code. Whenever you want to test code you have just modified, remember to
save the .mjs file, stop and restart the server. 

The server has the following logic:

- compute the name of the file that corresponds to the URL
- if the file exists, read and send the content
- if not, send an error message

For this exercise, add code in the server to detect if the URL is `/`.

If the URL is just `/`, send a small HTML content as a response,
announcing the server is up and running. Make sure the content type is `text/html`. 

If the URL is not `/`, run the previous code.

The new server logic will be:

- compute the name of the file that corresponds to the URL
- ***if the file name is empty ("") (or if the request URL is "/"), send a welcome message***
- if the file exists, read and send the content
- if not, send an error message

</div>

# Exercise 2

<div class = "activity">

Add code in the server to process the URL `/date`. In that case, send the current date and time (which you can get with
`new Date().toString()`). Make sure the content type is `text/plain`.

The new server logic will be:

- compute the name of the file that corresponds to the URL
- if the file name is empty ("") (or if the request URL is "/"), send a welcome message
- ***if the request URL is "/date", then send the current date of the computer*** 
- if the file exists, read and send the content
- if not, send an error message

Check that the URL `/` and file serving are still working in parallel with `/date`.
It is important to always check that what you did before is still working after your latest changes.

</div>

# Exercise 3

<div class = "activity">

Add code in the server to process URL /exit. In that case, the server should stop running. The function to call for this
is `process.exit(0)`

Check that `/`, `/date` and file serving are still working.

So you see that a web server can:

- serve files
- serve content created from nothing
- serve calculated content
- do something else on the machine, like stopping the server
- we will see later about receiving and storing data

</div>

# Exercise 4

<div class = "activity">

Now you see that the structure of the code of a usual web server is a long list of tests on the URL.

So rewrite your code to make that very clear and extensible:

* create one function with all the list of tests
* create one function for each case

Something like:

```javascript
function processRequest(request, response) {
    if (request.url ...) fun1(request, response);
    else if (request.url ...) fun2(request, response);
    else if (request.url ...) fun3(request, response);
        ...
    else funlast(request, response);
}
```

Then have createServer use your new `processRequest` function 
instead of the former function `webserver` and check that `/`, `/date` and file serving are still working.

Please give `fun1`, `fun2`, etc meaningful names. The function in the last else should be the one processing file service.

From now on, when you add a new URL, you add one line in `processRequest` and create one function to process that URL
elsewhere.

</div>

# Exercise 5

<div class = "activity">

Add code to you server to deal with:

- `/add?user=name`
- `/showlogs`

The intention of the first URL is to add a name and a date in a file. The intention of the second URL is to show the file with the names and dates.

Example: after running three URLs with `add`, a local file with name `logs.txt` looks like:

```text
user1 Wed, 30 Nov 2022 09:12:02 GMT
user2 Wed, 30 Nov 2022 09:13:12 GMT
user3 Wed, 30 Nov 2022 10:12:02 GMT
```

The syntax of URL `/add?user=name` is typical of forms on the web. The `url` module offers a function `parse` which
processes this type of URLs. 

- you will check that request.url starts with "/add" (with the function `.startsWidth()`)
- you import the module with `import {parse} from "url";`
- you use the module with `const query = parse(request.url, true);`
- you get the value of a parameter with `query.user` which is this case is `name`

When you receive `/add`, you add the user name and the date in a file called `logs.txt`. You can do this with the function
from the fs module called `appendFileSync(path, data)`. Add it to the import section.

When you receive `/showlogs`, you read the content of `logs.txt` and send it as plain text.

Test that as you call `/add` with different user values, the `/showlogs` shows all the values.

Check that all the previous URLs still work.

</div>

# Exercise 6

<div class = "activity">

You can see that now, a file called `date` in your main folder will not be served, as your server will send the current
date in response to `/date`. Same with add, showlogs, exit, etc. We will remove those name conflicts.

Change your code so that only a URL starting with "/file/" will serve existing files in the main folder, as before.

</div>

# Exercise 7

<div class = "activity">

The file `test.mjs` is in the main folder, and it is a security issue that it can be read from Internet.
Usually, file service is confined to a specific folder (not the one containing the server code).

Change your code so that only files from subfolder `www` of your main folder are served.

To be extra clear, file `www/test.txt` will be served when URL `/file/test.txt` is received.

You can thus see that the access URL and the file path can be very different.

</div>

# Using node.js on the Télécom computers

On the Télécom computers, there is a very old version of node.js which does not
treat modules well.

The easiest solution to use a modern node is to install `nvm`.

Copy this and paste it in a terminal on one of the Télécom computers:

`curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash`

Possibly, execute what is asked at the end of the previous process.
Then run:

`nvm install node`

This will install the latest version, compatible with modules (*.mjs files).

Doing this once on one of the Télécom computers will be enough while you are using the same account.

> On your own computer, you will probably install the latest version. If you have problems with modules, check the
> version of node with `node --version`. If the version is 10.* or below, install nvm as above.

