# Lab: Tester

<div class="activity">

The purpose of this lab is to study the concepts discussed during [INF203
on Web server technologies](http://perso.telecom-paris.fr/dufourd/cours/inf203/).

Testing a Web server can be done in at least two ways:

- using a browser and sending one test URL after another to the server, and checking everything the server returns
by using the Netwok tab of the browser debugger.
- writing a program that reads a list of URLs with options and automatically tests what the server returns.

We propose that you write such a tester. You will program in JavaScript, using NodeJS and
its support for HTTP / HTTPS protocols.
</div>

## To do

<div class="activity">

To program, use the "strict" mode of JavaScript, the indentation and
comments. 

***Thank you for using zip (and not tar and gzip or bzip2). Zip all
files directly into a single zip, do not zip the folder.***

***No spaces or accented characters or special characters in
the name of the zip file.***

***This lab is to be done alone***.

Please respect the file names and IDs we ask you
to use, otherwise automatic grading will not work and
you will not have the grade matching your work.

**Use local URLs in your files**, so your production
works regardless of the URL of the server, and so that it
also works on the grading machine.

In all your functions that respond to HTTP requests, put a `try
{} catch {}` to catch all the exceptions, and show the
error messages.

</div>

## Use of NodeJS modules (see annex 1)

## Useful code for later

The JS code for use in node.js to send an HTTP request has this structure:

```javascript
const req = http.request(
    URL, // the URL you want to request
    {},  // options, see the documentation
    response => { // this is the callback, i.e. the function that is called when the response has arrived
        // here goes the code you want executed on the response
    });
req.on("error", err => console.log(err)); // or do something specific on error
req.end();
```

First, you create a request object, then you set the listener for errors, then you start the requesting process by `end()`.
The callback will be executed last, after some time.

## Web Server Tester

<div class="activity">

The overall logic of your program is:

- the program takes a file name as command line argument; the file uses JSON to describe the URLs to test and what
the result should be
- the program loads the file with a list of tests
- the program executes the tests one by one
  - for each test, the program displays all the information received from the server
  - and checks the received values against a reference
  - prints Success or Failure accordingly
- the program displays a test summary
- and exits

But first, let us experiment with requests:

### Question 1: Send request then show statusCode 

This is a straightforward reuse of the code provided above.
Have a look at the [documentation of http.request](https://nodejs.org/api/http.html#http_http_request_url_options_callback){target="_blank"}
and the [documentation of response](https://nodejs.org/api/http.html#http_class_http_incomingmessage){target="_blank"}
Use console.log to display the statusCode.

Wrap this code in a function called `sendAndShowStatusCode(url)`

### Question 2: Send request then show header content-length

A bit more complicated: use console.log to show the value of the Content-Length header.

Wrap this code in a function called `sendAndShowContentLength(url)`

### Question 3: Send request then show content

One difficulty is that the callback is called as soon as the response arrives.
THe response may take some time to be complete, for example if the data is large.
So the content is not necessarily present when the callback is called.

Steps are:

1. when a chunk of data is received, the `data` event is called on the response
2. when the data is completely received, the `end` event is called on the response

The code to be added to the callback is:

```javascript
let data = "";
response.on('data', chunk => data += chunk) // accumulate chunks in data
response.on('end', () => ...) // ... is the code of what you want to do with data
```

So with all that, show the content received after the request.

Wrap this code in a function called `sendAndShowContent(url)`

### Question 4

Create the skeleton of the tester program, which just prints each URL to be tested.

Note: creating the best skeleton is the hardest task of this lab. It is very simple to launch all the requests
and wait for the result. However, some webservers may crash if you send too many requests at the same time.
Or if you are trying to debug the server, it will be hard to know which URL caused the problem.
Hence it is a good idea to send the first request, wait for the answer, then send the second request, etc.

The test format is:

```json
[
 { "url": "http://perso.telecom-paris.fr/dufourd/cours/" },
 ...
]
```

### Question 5

Add for each test the ability to send the (GET) request to the server. 
When receiving the response, the program displays the URL followed by the status code of the response.

### Question 6

Add for each test the ability to test the returned status code by adding the "statusCode" property.

### Question 7

Add for each test the ability to display all the HTTP headers received by adding the "printHeaders" property.

### Question 8

Add the checking of one header and its value, by adding the "checkHeaders" property, which takes as value an object.
Each property of the object is the name of an HTTP header, and the value of the property is the expected value of that
header.

### Question 9 

Add the checking of content received: add the property "checkTextContent" with a string value

### Question 10

Add the checking of content received with a file: add the property "checkContentFromFile" with the file name.
The file is possibly binary (e.g. image). The data in the file is checked against the content of the response.

### Question 11

Add the checking of HTTP requests other than GET: add the property "method" with value "GET", "POST", "PUT"...

### Question 12

Add the sending of content for POST and PUT: add the property "addTextContent" with a string value.

When there is data to be sent, the request code needs an additional line, right before the call to `end()`:

```javascript
req.write(data);
```

### Question 13

Add the sending of content for POST and PUT: add the property "addContentFromFile" with a file name.
The data of the file is sent as content in the HTTP request.

### Question 14

Add the sending of specific headers: add the property "addHeaders" with an object.
Each property of the object is the name of an HTTP header, and the value of the property is the value to be sent.

</div>

## Automatic grading

<div class="activity">

To grade your TP, you can just drop your zip below.

The zip file MUST be called `tpserver.zip`

>***How to get help on grading messages***
>
>If anything goes wrong in the process of grading your work, contact [me](mailto:dufourd@telecom-paris.fr){target="_blank"}.
>This grader keeps the last upload you have made for each lab.
>If you have a problem, I can instruct the grader to replay any of your labs, or take a look at your uploaded code.
>So you do not need to send me your JS code by email, which is forbidden by email security rules at Telecom.
>Just ***tell me which lab you are working on and which error message from the grader is blocking***.

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"><img src="https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png"/></a>

</div>

<script type="application/javascript">var currentGradingLab = "server";</script>
<iframe src="/images/dragdrop.html" style="width: 100%; height: 500px;"/>

## Annex 1: Use of Node.js

<div class="activity">

NodeJS is based on the concept of **modules**. A module is a block of
JavaScript code that you can load and use in your code,
that is to say as a library. To load a module, NodeJS
uses the `require` function. It is used like this:

```javascript
var module = require ('module_name');
```

There are many modules available for NodeJS. Some are
installed automatically with NodeJS (see the documentation
[Here](https://nodejs.org/api/)){target="_blank"}. In this case, you just have to use
`require` to use the module. This is the case for the `fs` module of
management of the "File System", used to read / write files
(see the documentation of this module
[here](https://nodejs.org/api/fs.html)){target="_blank"}, or even for the module
`http` which allows to receive and send HTTP messages (see the
documentation of this module [here](https://nodejs.org/api/http.html)){target="_blank"}.

NodeJS provides the command line tool `npm` to load more modules.
To download and install a module,
use the following command line:

```bash
> npm init //only do this line the first time
> npm install module_name --save
```

`npm init` creates a package.json. You can use the default
response to most questions.

The `--save` option adds a dependency to the `package.json` file which is required
for the grader to know that it needs to load this extra module.

If you have a problem with `npm`, for example it does not
exist on your system, look [there for instructions](#npm){target="_blank"}

You can verify that a module is installed by checking that
there is a subdirectory with the module name in the
`node_modules` folder. You can then use `require`.

For example,
to install the `http` module which is a simple http server,
use:

```bash
> npm install http
```

And then, in the JavaScript code, write:

``` 
var http = require('http');
```

</div>

## Annex 2: