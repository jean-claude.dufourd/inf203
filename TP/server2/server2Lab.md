# Lab: Server2

<div class="activity">

The purpose of this lab is to study the concepts discussed during [INF203
on Web server technologies](http://perso.telecom-paris.fr/dufourd/cours/inf203/).

</div>

## To do

<div class="activity">

For this lab, you will need to upload the different JavaScript files
you created. Everything will have to be zipped .

To program, use the "strict" mode of JavaScript, the indentation and
comments.

***Thank you for using zip (and not tar and gzip or bzip2). Zip all
files directly into a single zip, do not zip the folder.***

***No spaces or accented characters or special characters in
the name of the zip file.***

***This lab is to be done alone***.

Please respect the file names and IDs we ask for
to use, otherwise automatic grading will not work and
you will not have the grade matching your work.

Use local URLs in your files, so your production
works regardless of the URL of the server, and so that it
also works on the grading machine.

In all your functions that respond to HTTP requests, put a `try
{} catch {}` to catch all the exceptions, and show the
error messages.

Test your production by launching node in your main folder,
not in the server or client folder. If you are reading a "db.json" file
in the "server" folder, open it by reference to the path
"server/db.json" (not "db.json" or "../server/db.json").

</div>

## Use of NodeJS modules

<div class="activity">

NodeJS is based on the concept of **modules**. A module is a block of
JavaScript code that you can load and use in your code,
that is to say as a library. To load a module, NodeJS
uses the `require` function. It is used like this:

```javascript
var module = require ('module_name');
```

There are many modules available for NodeJS. Some are
installed automatically with NodeJS (see the documentation
[Here](https://nodejs.org/api/)){target="_blank"}. In this case, you just have to use
`require` to use the module. This is the case for the `fs` module of
management of the "File System", used to read / write files
(see the documentation of this module
[here](https://nodejs.org/api/fs.html)){target="_blank"}, or even for the module
`http` which allows to receive and send HTTP messages (see the
documentation of this module [here](https://nodejs.org/api/http.html)){target="_blank"}.

NodeJS provides the command line tool `npm` to load more modules. 
To download and install a module,
use the following command line:

```bash
$ npm install module_name
```

If you have a problem with `npm`, for example it does not
exist on your system, look [there for instructions](#npm){target="_blank"}

You can verify that a module is installed by checking that 
there is a subdirectory with the module name in the 
`node_modules` folder. You can then use `require`. 

For example,
to install the `express` module which is an http server,
use:

```bash
$ npm install express --save
```

And then, in the JavaScript code, write:

```javascript 
"use strict";

// °°vh
import express from "express"; 

const app = express();

// °°vk

// °°vi
app.get('/', (req, res) => res.send('Hello World!'))

// °°vj
app.listen(3000, () => console.log('Example app listening on port 3000!'))
```

> A little note: 
You will see that the `express` module is a lot simpler to use that `http` for some things, so why did I
ask you to use the `http` module in the previous lab ? On one hand, `express` does too much for you and hides
too much of the complexity that I want you to get to understand. On the other hand, `express`
tutorials on the web are full of VERY advanced stuff that could mess up your lab very quickly.
Usually, the advanced stuff is also VERY poorly documented.
So the recommandation is: use `express` when it is simpler than `http`, but if you see some info
about a "fantastic" middleware, be very cautious you do not end up spending hours on something
that should be simple...

</div>

## Exercice 1 - REST API

<div class="activity">

You will create a REST API on this [JSON database](../server2/db.json){target="_blank"}. An
REST-compatible API, or "RESTful", is a programming interface
application that uses HTTP requests to get (GET),
place (PUT), publish (POST) and delete (DELETE) data. 

File name: `server2.js`

**Question 0:** Create a server that just says `Hi`, listening to a port
number given on the command line. Then add simple logging of requests. 
You can use the module `morgan` for logging.

The command line will then be, to start the server on port 8000:
`node server2.js 8000`

To get the command line arguments use the array `process.argv`

Modify your server to respond to:

* `http://localhost:8000/°°va` to make the server stop and exit,
* `http://localhost:8000/°°vb` which simply reloads
db.json in memory, and answers in plain text "db.json reloaded".

The two are necessary for the automatic grading tool.

-----

**Question 1:** Create a server that, to the request
`http://localhost:8000/°°vc`, replies the number of publications
documented in db.json. This answer is plain text.

-----

**Question 2:** Add the request processing
`http://localhost:8000/°°vd/xxx`, which answers the number of publications
where the name of one of the authors contains xxx, ignoring the
case of letters. This answer is plain text.

After doing it simply by getting the `xxx` from the `request.url` field, 
note that express has a feature of naming fields in the URL, with prefix :
See the doc [here](https://expressjs.com/en/guide/routing.html){target="_blank"}

Then use that feature to simplify your code.

----- 

**Question 3:** Add the processing of the request
`http://localhost:8000/°°ve/xxx`, which answers the descriptors of
publications whose names of authors contain `xxx`, ignoring the case of letters.
This answer is in JSON, so should have the media type `application/json`

Note: the descriptor of a publication is the object describing
the publication, with all its properties. A descriptor is
one of the objects in the array in db.json.

Example:

```json
{
    "key": "8000304",
    "title": "Assessment of Fetal Exposure to 4G LTE Tablet in Realistic Scenarios: Effect of Position, Gestational Age and Frequency",
    "journal": "IEEE Journal of Electromagnetics, RF and Microwaves in Medicine and Biology",
    "year": "2017",
    "month": "aug",
    "keywords": "Fetus Dosimetry, Specific absorption rate, Radio frequency, Pregnancy, Electromagnetics, Antennas Author Keywords Radio frequency, Dosimetry, Polynomial chaos, Pregnancy",
    "lang": "en",
    "authors": [
      "E. Chiaramello",
      "M. Parazzini",
      "S. Fiocchi",
      "P. Ravazzani",
      "j. wiart"
    ],
    "category": "article",
    "state": "published",
    "dept": "comelec",
    "group": "rfm"
}
```

-----

**Question 4:** Add the processing of the request 
`http://localhost:8000/°°vf/xxx`, which answers the titles of
publications whose names of authors contain `xxx`, ignoring the case of letters.
This answer is in JSON, so should have the media type `application/json`

-----

**Question 5:** Add GET request processing
`http://localhost:8000/°°vg/xxx`, which responds the descriptor of the publication whose
"key" is `xxx`.

-----

**Question 6:** Add the processing of the DELETE request
`http://localhost:8000/°°vg/xxx`, which deletes the publication whose
"key" is `xxx` in the database that is in memory. No need to save
the database for this exercise. Check the deletion by querying the
base with °°vc, author, or titles.

To send a DELETE query, you can use the
command line:
`curl -X DELETE http://localhost:8000/°°vg/xxx`

-----

**Question 7:** Add the POST request
`http://localhost:8000/°°vg`, which adds an `imaginary` publication to 
the database that is in memory. No need to save
the database for this exercise. Check the addition by querying the
base with °°vg, °°vc, °°vd, or °°vf.

Please use `imaginary` as the key of this new publication.

A POST request is basically sending a multi-part mime structure with your data in it.

In order to process the upload of the new reference, you should use the package 
`body-parser` which will hide from
you the complexity of receiving the multipart mime and extracting the data from it.

To send the request, create a file called `createpost.txt` in which you will write 
the `curl` command that posts your new reference.

One question is: how to get the POST-ed data from the system. There is one express "middleware" that helps.

If you define:

```javascript
const bodyParser = require('body-parser');

app.use(bodyParser.json())
```

Then, you can use the field `request.body` to get the JSON data that was posted. This middleware would
allow you to get files, JSONP, XML and other formats by changing the option of the `body-parser` module.

There are other middlewares that do it differently, `formidable` for example.

-----

**Question 8:** Add the PUT request
`http://localhost:8000/°°vg/xxx`, which changes the publication whose
the "key" is `xxx` in the database that is in memory. No need to save
the database for this exercise. Check the addition by querying the
base with °°vc, °°vd, or titles.

A PUT request is basically sending a multi-part MIME structure with your 
data in it, like POST.

In the PUT JSON data, just add the fields that you are modifying. Fields already present and not in PUT data
will be kept as they are.

To send the request, create a file called `createput.txt` in which you will
 write the `curl` command that
puts your modified reference.

</div>

## Automatic grading

<div class="activity">

To grade your TP,  you can just drop your zip below.

The zip file MUST be called `tpserver2.zip`

>***How to get help on grading messages***
>
>If anything goes wrong in the process of grading your work, contact [me](mailto:dufourd@telecom-paris.fr){target="_blank"}.
>This grader keeps the last upload you have made for each lab.
>If you have a problem, I can instruct the grader to replay any of your labs, or take a look at your uploaded code.
>So you do not need to send me your JS code by email, which is forbidden by email security rules at Telecom.
>Just ***tell me which lab you are working on and which error message from the grader is blocking***.

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"><img src="https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png"/></a>

</div>

<script type="application/javascript">var currentGradingLab = "server2";</script>
<iframe src="/images/dragdrop.html" style="width: 100%; height: 500px;"/>
