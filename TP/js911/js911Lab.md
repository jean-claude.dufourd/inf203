# Lab: JS for designers

<div class="activity">

The purpose of this lab is to study the concepts discussed during [INF203
on Web technologies](http://perso.enst.fr/dufourd/cours/inf203/) and in particular
JavaScript. The goal of this lab is to program in JavaScript without
using the browser. Another lab session will concern JavaScript in the
browsers. 

</div>

# Preamble - Using Node JS

<div class="activity">

JavaScript is a "complete" programming language. You can
program in JavaScript everything you could do with any other programming
language (Python, Java, C, ...).

[NodeJS](https://nodejs.org/en/){target="_blank"} is a runtime environment of
JavaScript code. NodeJS uses the same JavaScript engine as Google
Chromium. It can be used on the server side to generate web pages. It
can also be used on the command line (like python for example) to
run a program. That's what we will do.

On machines in the TPT lab rooms, you can use it as follows:
the `>` symbol at the beginning of the line refers to the NodeJS prompt,
i.e. where NodeJS waits until you input code and press Enter, and
the `$` symbol refers to the Unix command prompt; the other lines
are the results of the execution.

```
$ nodejs
> console.log ('Hello World');
Hello World
undefined
>
```

The first line displays the desired result. The second displays
the return value of the `console.log` function which is
`undefined`.

This way of using NodeJS is very convenient for testing short programs, one line.
This becomes more complicated for several lines.
For this purpose, NodeJS accepts the name of a file as parameter. For example,
if the `hello.js` file contains:

```
console.log ('Hello World');            
```

The use of this file is simply:

```
$ nodejs hello.js
Hello World
$            
```

NodeJS also offers the possibility to debug your code, set breakpoints,
inspect variables and stack, etc. To use the debugger, run node with the
command:

```
$ nodejs debug hello.js
```

The debug mode documentation is located [here](https://nodejs.org/api/debugger.html){target="_blank"}.

At the top of each file, put `"use strict";` as a first line

If you have messages like "SyntaxError: Block-scoped
declarations (let, const, function, class) not yet supported outside
strict mode" then the "use strict"; at the top of your file is missing.

</div>

# Using node.js on the Télécom computers

On the Télécom computers, there is a very old version of node.js which does not
treat modules well.

The easiest solution to use a modern node is to install `nvm`.

Copy this and paste it in a terminal on one of the Télécom computers:

`curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash`

Possibly, execute what is asked at the end of the previous process.
Then run:

`nvm install node`

This will install the latest version, compatible with modules (*.mjs files).

Doing this once on one of the Télécom computers will be enough while you are using the same account.

> On your own computer, you will probably install the latest version. If you have problems with modules, check the 
> version of node with `node --version`. If the version is 10.* or below, install nvm as above.

# Exercise 0

<div class="activity">

In a file named `test.js`, paste the following code:

```javascript
"use strict";

function inc(i) {
    return i+1;
}

console.log("The result of inc(4) is "+inc(4));
```

Then in the terminal execute the following command:

```bash
node test.js
```

You should see: "The result of inc(4) is 5" in the terminal.

I explain the sections of the code with comments:

```javascript
"use strict";
// first line says to the JS interpreter to be strict

// next is the definition of the inc function
function inc(i) {
    return i+1;
}
// in general, before this, you should put all of your code in functions

// then comes the use of the functions defined above
// console.log executes its argument and prints the result on the terminal
console.log("The result of inc(4) is "+inc(4));
// you may write as many tests as you want for your function
```

Your code for the following exercises should be structured in a similar way.

</div>

# Exercise 0b

<div class="activity">

Here is a version of the template which takes the value to test on the command line:

```javascript
"use strict";

function inc(i) {
    return i+1;
}

console.log("The result of inc(4) is "+inc(+process.argv[2]));
```

I replaced the constant 4 with `process.argv[2]`. `process.argv` is the list of all command line arguments.
`process.argv[0]` is node.
`process.argv[1]` is test.js.
`process.argv[2]` is the value you type after. Because it is seen as a string, I change it to a number by adding + in front.
Do not put a + if the value is a string.
If the value you want has spaces in it, you need to put " " around your value.
</div>

# Exercise 1

<div class="activity">

Write a file `rep.js` with a function `rep` which replaces the first instance of a string in another string.
For example, in the string "the quick brown fox jumps over the lazy dog", replacing "fox" with "monkey" will yield
"the quick brown monkey jumps over the lazy dog"

Use the function `replace` which can be applied to a string.

</div>

# Exercise 2

<div class="activity">
 
Modify exercise 1 and rep.js into rep2.js which takes the value on the command line. 
You need 3 arguments: from string (`process.argv[2]`), to string (`process.argv[3]`), and main string (`process.argv[4]`).
Experiment with different values.

</div>

# Exercise 3

<div class="activity">

Modify exercise 2 and rep2.js into rep3.js to replace all instances of the `from` string appearing in the `main` string.
For example `node rep3.js blue red "fish blue fish blue fish` should yield "fish red fish red fish"

There are two ways to implement this function:

- use the string function `indexOf` to test if the string `from` is present in the string `main`, and while `indexOf` is not negative, call `replace`
- use a regular expression

</div>

# Exercise 4

<div class="activity">

Create a file `cat.mjs` which reads a file whose filename is given on the command line, and displays the content of 
the file (assumed to be text)

You can read the content of a file with the function readFileSync from module fs.
To use that function, include `import {readFileSync} from "fs";` at the beginning of `cat.mjs` (after the "use strict";)

</div>

# Exercise 5

<div class="activity">

Create a file `readjson.mjs` which reads a JSON file whose filename is given on the command line, and prints out
the ***keys*** and ***values*** found in the JSON file. The JSON file is assumed to contain only one object with keys and simple
values (values are not objects or arrays).

You can read the content of a file with the function `readFileSync` from module `fs`.
To use that function, include `import {readFileSync} from "fs";` at the beginning of `cat.mjs` (after the `"use strict";`)

You can transform a JSON string `str` into a JavaScript object by using the function `JSON.parse(str)`.

You can create a loop on all keys in an object `obj` with the syntax:

```javascript
for (var key in obj) {
   console.log(obj[key]); // this prints all the values only
}
```

</div>

# Exercise 6

<div class="activity">

You have names and adresses in this JSON format. This is one entry:

```JSON
{
  "firstname": "Jean-Claude",
  "name": "Dufourd",
  "address": "8 rue Robert Marchand",
  "postalcode": "78200",
  "city": "Cachan"
}
```

And you have a template of an email in HTML:

```html
<!doctype html>
<head><meta charset="UTF-8"/></head>
<html>
<body>
<p align="right">Palaiseau, _date_</p>
<p>To: _firstname_ _name_,</p>
<p>_address_</p>
<p>_postalcode_ _city_</p>
<br>
<p>Dear Mr or Mrs _name_</p>
<p>Content of this letter</p>
<p>Sincerely</p>
<p>The dean of Télécom Paris</p>
</body>
</html>
```

Write a program that reads the name entry in the file given as first argument, the email template in the file given as
second argument, and generate the email:

- replace _firstname_ with the value of firstname in the entry,
- same for all properties of the entry,
- replace _date_ with the current date (with `new Date().toDateString()`).

You will start with displaying the email on the terminal, then when it works, modify the code to write the email
in a file with writeFileSync (from module fs)

</div>

# Exercise 7

<div class="activity">

There is now a database of entries, as an array of entries.
The database is an array of entries. Arrays in JSON are noted with []. Array entries are separated with commas.

Write a program that reads the database in the file given as first argument, the email template in the file given as
second argument, and generate one email per entry in the database.

Each email is written to a file named `emailX.html`, X being the number of the entry

</div>

