# Lab: HTML

<div class="activity">

The purpose of this lab is to study the concepts discussed during [INF203
on web technologies](http://perso.telecom-paris.fr/dufourd/cours/inf203/) and in
particular HTML, CSS and SVG.

Be careful: in this lab, there are often a lot of ways to get a
visually correct result, and even a lot of ways to make a
legal result in the sense of HTML. But there are much fewer
good ways to do it, in the spirit of HTML5. In this lab session, we will
stick with HTML5 spirit and so the grader will refuse you many solutions
that may work visually but are not what we are looking for.

</div>

## Simple use of HTML

<div class="activity">

Question 1a: Follow [this link](exercise1.html){target="_blank"} with your browser,
use the source code browser ("View Source" or CTRL + U on Windows, 
"View Page Source" or Alt-Command-U on a Mac) and
the Inspector Element (F12 or Alt-Command-I) and compare the
results. What are the differences ?
So get in the habit of regularly checking if the console displays
errors and if the interpretation made by a browser is consistent
to your expectations.

Remember there are three elements in question 1a:

1. the text file named exercise1.html with just one string in it,
2. the display in your browser where you see just the string,
3. the web developer window where you see the interpretation of the
text file by the browser.

(2.) is not editable. (3.) is editable, but will disappear as soon as you reload
the page from the file.  

Question 1b: In the **file** of the previous question, add the
basic tags then put in bold the words Telecom Paris. Save your
result in the file `exercise1b.html`. 

Warning: the web developer window (3.) is editable and you could do the changes
there and see the results in 2. but there is no way to save these changes.
You have to edit a text file, for example using the Atom editor on the
lab machines.

Question 1c: Add a "title" tag containing the text: "This is
a test page", and save it to the file `exercise1c.html`.
What is displayed in the browser? 

Question 1d:
In the previous file, place the text "My personal page from
Telecom Paris" in a `h1` element at the beginning of the page.
After the `h1` element, create 3 `div` elements and insert text into
each of these elements. Insert enough text in the first two so that the complete content
does not fit in the page and that the scroll bar
appears. You can use automatically generated text, for
example of the Latin "Lorem ipsum" with [this online generator](http://fr.lipsum.com/)){target="_blank"}. 
Give identifiers `div1`,
`div2`, `div3` to each `div` element respectively. Note: the tag has to be `div` 
and the identifier is the value of an attribute called `id`. 

Create a bullet list to navigate the page, with one link
targetting each of the three `div` elements. Place
this list in a `nav` element before the `div` elements. 
Add an external link (to another website) in the bullet 
list at the end, so that you have four children in the `nav`.

Add an image at the beginning of the second `<div>`. Use an image in
PNG, JPEG or SVG format.

Save the result to a file named `exercise1d.html`.

Question 1e: Experiment the robustness and behavior of browsers when your
tags are not or are badly closed. Find an example of an unclosed tag
that does not change anything, and another unclosed tag that changes
rendering dramatically.

</div>
 
## Adding CSS Styles

<div class="activity">

Question 2a: Add to the file `exercise1d.html` via style attributes:

- on the first div, put CSS code that defines a 1 pixel
thick border. Add space (10px) between the text and the border.
- on the image of the second div, add CSS code so that the text is displayed 
to the right of the image, with the rest of the text below the image if it 
is longer than the image. Think about readability, and so
put a little space (10px) between the text and the image.
- on the third div, add CSS code that defines a background color
`#CCCCCC` (which is a grey). Add space (10px) around the grey rectangle.
Do not use certain color names, even if they work in the browser,
they break my grader.

As a reminder, you can check and modify the CSS code via
the code inspector until you get it right, then transfer changes
to the text file.

Save the result to a file named `exercise2a.html`.

Question 2b: Move these CSS styles into a `<style>` element.
Save the result to a file named `exercise2b.html`.
Explain what you had to change and the advantages / disadvantages of
this method.

Question 2c: Then delete this `<style>` element and use a
external style sheet named `exercise2c.css`. Save the
result in a file named `exercise2c.html`. Explain what you
had to change and the benefits of using an external style sheet.

Question 2d: Add a class to the third div. Change the CSS of
this class for this div to be displayed on other divs in
position 100px, 100px (relative to the top left of the page) with a
width and height of 400px. Save the result to files
named `exercise2d.html and exercise2d.css`.

Question 2e: Change the CSS of this class so that this div is
displayed on the other divs in position 100px, 100px with a width
and height of 400px, but so that it does not move with the rest of the
text when you scroll the page with the mouse. Save the result to
files named `exercise2e.html and exercise2e.css`.

Question 2f: Change the CSS of this class so that this div
disappears when you click on it. Note: the div comes back when you
release the mouse button. Save the result to files
named `exercise2f.html and exercise2f.css`.

Question 2g: Change the CSS of this class so that the text of this
div goes red when you hover over it without clicking.
Save the result to files named `exercise2g.html` and
`exercise2g.css`.

</div>

