# TP supplémentaire CPD

Pour chaque exercice, créez un nouveau fichier, eventuellement en recopiant
un autre fichier.

## Tables

* Créez une table simple avec 3 colonnes et 6 lignes, la première
ligne étant une barre de titre (ou entete) stylée différemment
* Créez une table avec des couleurs de fond alternées par ligne
* Créez une table avec des bords de 3 types différents
* Créez une table avec un pied de table
* Créez une table avec des cellules fusionnées et des tailles de lignes et de
colonnes différentes

## Media

* Créez une page avec une video qui joue automatiquement en boucle
* Créez une page avec une video avec des contrôles, démarrable par 
l'utilisateur
* Créez une page avec trois pistes audio et les contrôles pour les jouer

## Formulaires

* Créez une page avec un formulaire contenant tous les types d'input et 
un selecteur
* Ajouter un style à chaque type d'input

## Objets spéciaux

* Créez une page utilisant des details
* Créez une page utilisant un pre
* Créez une page utilisant un blockquote
* Créez une page utilisant sup et sub
* Créez une page utilisant ol li ul et changer le type de puce et de nombre
* Créez une image ronde
* Créez un menu avec des items qui pointent vers d'autres de vos pages
* Créez une barre de navigation horizontale 

## Position

* Créez une page avec un layout en 2 colonnes (20%, 80%)
* Créez une page avec un layout en 3 colonnes
  * la colonne de gauche fait 100 pixels
  * la colonne du centre fait 50%
  * la colonne de droit fait le reste
* Créez une page avec une barre de titre et une barre de pied de page qui
sont inamovibles (ne scrollent pas)

## Media Queries

* Créez une page qui a trois colonnes au dessus d'une certaine taille écran
et une seule colonne en dessous, avec les mêmes contenus
* Créez une page qui a trois colonnes au dessus d'une certaine taille écran
et une seule colonne en dessous, avec des textes résumés et moins d'images

## Une page dans une autre

* Créez une page qui contient une autre page dans un rectangle (iframe)

## Bootstrap

* Clonez votre page de formulaires, ajoutez bootstrap, et ajoutez des classes
pour bien décorer votre formulaire. Vous trouverez les classes CSS à utiliser
dans les exemples de widgets Bootstrap. Faites plusieurs essais jusqu'à ce
que vous soyez content du résultat. Eventuellement, faites plusieurs versions
de style pour des usages différents avec la même structure de base.

## Caroussel, onglets...

* Créez des onglets sans JS (voir par exemple
[ce lien](https://stackoverflow.com/questions/6906724/is-it-possible-to-have-tabs-without-javascript){target="_blank"} 
ou [ce site](
   https://www.sitepoint.com/you-dont-need-javascript-for-that/))
* Créez un carousel d'images sans script d'après [ce site](
https://www.sitepoint.com/you-dont-need-javascript-for-that/) 

## SVG

* Créez un petit fichier SVG avec l'éditeur de texte, avec juste un ou deux
objets graphiques. Ouvrez ce fichier dans le navigateur et vérifiez son
rendu. N'oubliez pas l'attribut viewbox du svg...
* Insérez ce fichier dans un fichier HTML en utilisant un tag img avec un
attribut width="100" et height="200".
* Insérez ce fichier dans un fichier HTML en utilisant un tag img avec un
attribut width="200" et height="100". Voyez la différence de rendu
avec le HTML précédent. Modifiez le fichier SVG pour que le SVG soit rendu 
sur la droite. Modifiez ensuite le fichier SVG pour que les objets 
remplissent tout l'espace (200x100) dans le HTML. Ca n'est possible que si
le viewbox est rempli par les objets SVG.
* Insérez ce fichier SVG directement en copiant le texte dans un fichier
HTML existant. Ouvrez le fichier HTML et voyez le rendu du SVG dans le 
fichier HTML. Eventuellement, ajustez. Ajoutez une div avec un fond coloré 
et un bord autour du SVG. Pour la suite, vous pourrez utiliser les
fichiers SVG indifféremment seuls ou insérés dans un HTML.

* Créez un SVG avec un unique objet path qui dessine une fleche.
* Créez un SVG avec un unique objet path qui dessine un trèfle à quatre 
feuilles, en utilisant des arcs.
* Créez un SVG qui reprend le trèfle et le fait tourner sur lui-même 
pendant 10s à un tour par seconde.
* Créez un fichier HTML avec une liste à puces, et utilisez le trèfle en
SVG comme puce.

* Créez un fichier HTML qui affiche un texte sur un cercle.
* Créez un SVG avec un graphe style variations d'une action à la bourse.
