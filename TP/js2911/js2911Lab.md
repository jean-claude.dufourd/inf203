# Lab: Javascript in the browser

<div class = "activity">

The purpose of this lab is to study the concepts discussed during [INF203
on Web technologies](http://perso.enst.fr/dufourd/cours/inf203/) and in particular
JavaScript. The objective of this lab is to program in JavaScript in the
browser.

> Unlike previous labs, this one will not work if you
> stay "local", i.e. if you leave all your files in
> a folder and you access them directly by "Open File"
> in the browser.
> You need to install a web server and use a folder in its server root, for example.

You can use the beginning server of the previous lab:

```javascript
"use strict";

import {createServer} from "http";
import {readFileSync, existsSync} from "fs";
import pkg from "mime";
const {getType} = pkg;

function webserver( request, response ) {
  const filepath = request.url.substring(1); // remove initial /
  if (existsSync(filepath)) {
    response.setHeader("Content-Type", getType(filepath));
    response.end(readFileSync(filepath));
  } else {
    response.statusCode = 404;
    response.end("the file " + filepath + " does not exist on the server");
  }
}

const server = createServer(webserver);

server.listen(process.argv[2], (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("server started on port "+process.argv[2]);
  }
});
``` 

See usage instructions in <a href="../server911/server911Lab.html">here</a>

</div>

# Assignment

<div class = "activity">

For this lab, you will need to upload the different JavaScript files
products and all the files necessary for their execution (HTML,
resources). Use the "strict" mode of JavaScript, indentation and comments.

***This work is to be done individually.***

For all the exercises on Ajax, you need to check that you have all the elements:

- a web server to send the HTML to the browser: if loaded from file, the browser refuses to execute the AJAX code
  - on this server, there should be an HTML file, maybe a CSS file, and surely a JS file
- a web server to send the content that AJAX will request: it could be the same server

</div>

# Exercise 1

<div class = "activity">

The intent of this exercise is to build the simplest AJAX example.

Here is an HTML file with a button and div to place the content loaded with AJAX:

```html
<!doctype html>
<html>
<head>
    <meta charset='utf-8'>
    <title>test ajax</title>
</head>
<body>
    <button type="button" onclick="ajax()">Request</button>
    <br>
    <div id="content"></div>
    <script type="application/ecmascript" src="ajax.js"></script>
</body>
</html>
```

Here is the JS file which should be named `ajax.js` and be in the same folder as the HTML.

```javascript
"use strict";
function ajax() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "ajaxtest.txt");
    xhr.onload = function() {
        document.getElementById("content").textContent = this.responseText;
    }
    xhr.send();
}
```

You choose what you put in file `ajaxtest.txt`, but place it in the same folder as the HTML.

</div>

# Exercise 2

<div class = "activity">

Create an HTML page with 4 buttons and one div with an id.

The action of the first button will be to add a piece of text (anything) as textcontent to the div.

The action of the second button will be to add after the div, another div with another text (anything else).
To do that, you find the div with the id with `document.getElementById`as above, you get the .parent of that node, 
then apply `appendChild` to the parent. To create a div, you can use `document.createElement("div")`.

The action of the third button will be to put a style on the first div: color: `red;`. To do this, you can access the 
.style property of a DOM Node, and in .style you can access .color, which you can set to 'red'. 

The action of the fourth button will be to remove the second div, if it exists. If you put an id on that second div
when you create it, then you can find that object and call `remove()` on it.

</div>

# Exercise 3

<div class = "activity">

We will create a simple animation: every second, a new random number will be displayed on the page.

`Math.random()*1000000` gives you a random number between 0 and 999999. Create a button that calls a function
that replaces the textcontent of a div with that number.

Now, we want an infinite loop, with a one second wait. The function `setTimeout` calls a certain function after some 
time. The first parameter is the function, the second is the time in milliseconds.

Here is a prototype of infinite loop:

```javascript
function loop() {
    // some action such as drawing a turning wheel
    ...
    setTimeout(loop, 1000);
}
```

If the function loop is called once, it will call itself every second after that. Reload the HTML to stop it.

So use all this information to create an infinite loop where every second, a new random number will be displayed on the page.

</div>

# Exercise 4

<div class = "activity">

> You need to have done the server911 lab before this.

We will reuse your server with `/add` and `/showlogs` from the previous lab to create a simplified chat room.

The first part is about sending information from your page to the server with the `/add` URL.

Extend the above HTML template to add a text input: `<input type="text" .../>`

Change the action of the button to a function which takes the content of the text input, creates a URL starting with
`/add?user=` followed by the content of the text input, and sends the request. In this cas, onload is not important
since we do not expect a response.

Start your server and test.

</div>

# Exercise 5

<div class = "activity">

The next part is to use `/showlogs` and put the content in a div, every second, and to see it change when you use the
button of exercise 4.
So it is a loop like in exercise 3, an ajax request to `/showlogs` like in exercise 1, set into the context of exercise 4.

</div>

# Exercise 6

<div class = "activity">

Find some nice pictures on the web. Put them on your server. Create a JSON file with the list of the URLs of the pictures.
Create an HTML page to load the list of pictures, then display, full screen, randomly, one of those
pictures, changing every 3s. Use the functionality of the previous exercises.

</div>

# Using node.js on the Télécom computers

On the Télécom computers, there is a very old version of node.js which does not
treat modules well.

The easiest solution to use a modern node is to install `nvm`.

Copy this and paste it in a terminal on one of the Télécom computers:

`curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash`

Possibly, execute what is asked at the end of the previous process.
Then run:

`nvm install node`

This will install the latest version, compatible with modules (*.mjs files).

Doing this once on one of the Télécom computers will be enough while you are using the same account.

> On your own computer, you will probably install the latest version. If you have problems with modules, check the
> version of node with `node --version`. If the version is 10.* or below, install nvm as above.
