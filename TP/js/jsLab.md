# Lab: JavaScript basics

<div class="activity">

The purpose of this lab is to study the concepts discussed during [INF203
on Web technologies](http://perso.enst.fr/dufourd/cours/inf203/) and in particular
JavaScript. The goal of this lab is to program in JavaScript without
using the browser. Another lab session will concern JavaScript in the
browsers. 

This lab session is graded automatically and the grading tool is
available so that you can measure where you are. See at the end.

</div>

## Assignment

<div class="activity">

For this lab, you will need to upload the different JavaScript files
products. Use the "strict" mode of
JavaScript, indentation and comments. 

***Thank you for using zip (and not tar and gzip or bzip2). Zip the files
directly, do not zip the folder.***

***No spaces or accented characters or special characters in
the name of the zip file nor in folder names.***

***This assignment is to be done individually.***

</div>

## Preamble - Using Node JS

<div class="activity">

JavaScript is a "complete" programming language. You can
program in JavaScript everything you could do with any other programming 
language (Python, Java, C, ...).

[NodeJS](https://nodejs.org/en/){target="_blank"} is a runtime environment of
JavaScript code. NodeJS uses the same JavaScript engine as Google
Chromium. It can be used on the server side to generate web pages. It
can also be used on the command line (like python for example) to
run a program. That's what we will do.

On machines in the TPT lab rooms, you can use it as follows: 
the `>` symbol at the beginning of the line refers to the NodeJS prompt,
 i.e. where NodeJS waits until you input code and press Enter, and 
 the `$` symbol refers to the Unix command prompt; the other lines 
 are the results of the execution.
 
```
$ nodejs
> console.log ('Hello World');
Hello World
undefined
>
```

The first line displays the desired result. The second displays
the return value of the `console.log` function which is
`undefined`.

This way of using NodeJS is very convenient for testing short programs, one line. 
This becomes more complicated for several lines.
For this purpose, NodeJS accepts the name of a file as parameter. For example,
if the `hello.js` file contains:

```
console.log ('Hello World');            
```

The use of this file is simply:

```
$ nodejs hello.js
Hello World
$            
```

NodeJS also offers the possibility to debug your code, set breakpoints, 
inspect variables and stack, etc. To use the debugger, run node with the
command:

```
$ nodejs debug hello.js
```

The debug mode documentation is located [here](https://nodejs.org/api/debugger.html){target="_blank"}.

At the top of each file, put `"use strict";` as a first line

If you have messages like "SyntaxError: Block-scoped
declarations (let, const, function, class) not yet supported outside
strict mode" then the "use strict"; at the top of your file is missing.

</div>

# Using node.js on the Télécom computers

On the Télécom computers, there is a very old version of node.js which does not
treat modules well.

The easiest solution to use a modern node is to install `nvm`.

Copy this and paste it in a terminal on one of the Télécom computers:

`curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash`

Possibly, execute what is asked at the end of the previous process.
Then run:

`nvm install node`

This will install the latest version, compatible with modules (*.mjs files).

Doing this once on one of the Télécom computers will be enough while you are using the same account.

## Exercise 1 - Numbers, loops, arrays, functions and recursion

<div class="activity">

Save the results of this exercise to a file named
`exercise1.mjs`.

Here is a template of `exercise1.mjs`:

```javascript
"use strict";

// °°vl
export function °°va(n) {}

// °°vm
export function °°vb(n) {}

// °°vn
export function °°vc(t) {}

// °°vo
export function °° vd(t) {}
```

To test the above, use another file like this:

```javascript
"use strict";

import {°°va,°°vb,°°vc,°°vd} from "./exercise1.mjs";

console.log(°°va(7)); // do more that one test per function
console.log(°°vb(8));
console.log(°°vc([3,5]));
//...
```

Question 1a: Write a function named `°°va` which calculates the
nth number of the Fibonacci sequence iteratively (with a for-loop or
while). fib(0) = 0, fib(1) = 1, fib(n) = fib(n-1) + fib(n-2)

Question 1b: Write a function named `°°vb` which calculates the Fibonacci sequence
recursively.

Question 1c: Write a function `°°vc` that takes an
array of numbers and that returns the array of results of
°°vb called on the numbers (without using the JS function `map`).

Question 1d: Write a function named `°°vd` equivalent to the
1c function using the JS function `map`.

Add at the end of the `exercise1.mjs` file (to allow 
automatic grading):

```
exports.°°va = °°va;
exports.°°vb = °°vb;
exports.°°vc = °°vc;
exports.°°vd = °°vd;
```

</div>

## Exercise 2 - Strings, Objects, Anonymous Functions

<div class="activity">

Save the results of this exercise to a file named
`exercise2.js`.

Question 2a: Write a '°°ve' function which, for each word
within a string, counts the number of occurrences of this word in
this string. The function shall return an object whose properties are
the words and the values ​​of these properties are the occurrence numbers. 
Make sure this function works on a string of at least 500 words. 
The input string is assumed to contain no punctuation and only small caps.

Example: on the string "fish bowl fish bowl fish", the result is `{fish: 3, bowl:2}`

Question 2b: Write a constructor '°°vf' which takes as input a
string and that returns an object with the following methods:

- maxCountWord () which returns the word with the most occurrences, and
if there are several words with the same number of occurences, returns 
the first of them in the lexicographically sorted list from getWords()
- minCountWord () which returns the word with the least number of occurrences, and
if there are several words with the same number of occurences, returns 
the first of them in the lexicographically sorted list from getWords()
- getWords () which returns an array of words present
in the original text, lexicographically sorted and without duplicates,
- and getCount (word) which gives the number of occurrences for a given word.

Question 2c: Add to °°vf an 'applyWordFunc (f)' method
to apply any function to each word and to
return an array of results, for example:

`function f (word) {return word.length;}`

`wordList.applyWordFunc(f)` then returns the array of lengths of
words

Add at the end of the exercise2.js file:

```
exports.°°ve = °°ve;
exports.°°vf = °°vf;
```

</div>

## Exercise 3 - Object-Oriented Programming in JavaScript

<div class="activity">

Save the results of this exercise to a file named
`exercise3.js`

Question 3a: Create a "°°vg" class with a constructor that takes
in "lastName", "firstName" and "id" as arguments, so that one
can write the following code:

    var student = new °°vg ("Dupond", "John", 1835);

Question 3b: Add a `toString` method that does not take a parameter,
and which returns a string of characters built to
from the properties of the object in the form:

    "student: Dupond, Jean, 1835"

Question 3c: Create a °°vh derived class that also allows
to give a nationality to a student, so the arguments of the
constructor are "lastName", "firstName", "id" and "nationality". 
Add a `toString` method that
takes the result of the basic class and adds the nationality, with
a field of name "nationality" as follows:

    "student: Doe, John, 432, American"

Add at the end of the `exercise3.js` file:

```
exports.°°vg = °°vg;
exports.°°vh = °°vh;
```

</div>

## Exercise 4 - Module in JavaScript

<div class="activity">

Save the results of this exercise to a file named
`exercise4.js`.

Reuse the classes of Exercise 3 by writing:

```
let ex3 = require ('./ exercise3');
let °°vg = ex3.°°vg;
let °°vh = ex3.°°vh;
```

In the console, execute the command: `npm install fs`

This installs the standard module fs to read and write
files. If you have a problem with `npm`, for example it does not
exist on your system, look [there for instructions](#npm){target="_blank"}

Look for the `fs` module documentation to get an idea of the
content of this module and how you will be able to use it in the
next exercise.

Question 4: Create a module for a °°vi class. 
An object of the °°vi class may contain
any number of °°vg or
°°vh objects and have
following methods:

  - add(student) which adds a student to the promotion,
  - size() which returns the number of students in the promotion,
  - get(i) who returns the i-th °°vg in the promotion in the order
    where you did add. `Note: the index of the first student is 0`
  - print() that prints all students to the console, one per line, 
  and returns the printed string
  - write() which serializes the promotion to JSON, in other words transforms
    a promotion object in a string of characters,
  - read(str) that reads a JSON object and rebuilds a promotion, WARNING: going through JSON.stringify then 
  JSON.parse looses the fact that the object was a new °°vg...
  - °°vj(fileName) that writes a promotion to a text file
    as a JSON object (reuse the above function write)
  - °°vk(fileName) which recreates a promotion from what has
    been saved to a file (reuse the above function read)

The functions write and read serialize and deserialize the object.
Please use JSON.stringify to serialize and JSON.parse to deserialize.

***Note: The grader actually assumes the °°vi is an array of student
objects.***

Add at the end of the `exercise4.js` file:

```
exports.°°vi = °°vi;
```

</div>
 
## Evaluation

<div class="activity">

The automatic grader is based on the concept of unit tests.
I wrote simple tests for all the functions that I have
asked you to program. The unit test framework is called mocha and calls all
tests in the `test` folder.

You can use
the LabGrader to grade your work.
You can use the grader as often as you need.
Only the last grade will count. 

You have to **submit** your work by dropping your zip below.

The zip file MUST be called `tpjs.zip`

>***How to get help on grading messages***
>
>If anything goes wrong in the process of grading your work, contact [me](mailto:dufourd@telecom-paris.fr){target="_blank"}.
>This grader keeps the last upload you have made for each lab.
>If you have a problem, I can instruct the grader to replay any of your labs, or take a look at your uploaded code.
>So you do not need to send me your JS code by email, which is forbidden by email security rules at Telecom.
>Just ***tell me which lab you are working on and which error message from the grader is blocking***.

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"><img src="https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png"/></a>


</div>

<script type="application/javascript">var currentGradingLab = "js";</script>
<iframe src="/images/dragdrop.html" style="width: 100%; height: 500px;"/>
