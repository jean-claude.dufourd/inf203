# INF203 - Web Technologies

<h3>Pedagogic objectives</h3>

The purpose of this course is to familiarize students with the
Web development techniques. There are courses on
formats and essential tools, graded implementation labs and
course of "culture" on the advanced web dev.

At the end of the course, students will:

- have used formats (HTML, CSS, JS) and common tools (node.js, serveur HTTP) of the web in labs
- know the basics of formats and common tools on the web
- understand the complexity of common features: forms, chat, upload, REST APIs
- have been exposed to notions of crawling, web security and trends in rapid
development libraries for the web.

<h3>Evaluation</h3>

Labs include implementation of formats and basic tools, and are almost automatically graded.

A quizz evaluates the learning in "culture" courses.
You have to connect to <a href="https://ecampus.paris-saclay.fr/course/view.php?id=8389" target="_blank">the course on ecampus</a> to have access to the quiz.

# Prerequisites

This course was initially built for second year students of Telecom Paris.
In the first year of TP, students come with foreknowledge of python and
learn OO programming in Java, then C, etc. My JS course teaches elements
in comparison with other already known elements from Java & C. 

This course is now included in other syllabi where students may be new to
programming. 

If you are new to programming, here are some resources for you:

* <a href="https://scrimba.com/course/gintrotojavascript" target="_blank">Scrimba JS beginner course</a>
in English (actually in American), very practical. This is rated for 2 hours
so it is an introduction to make my JS course accessible.
* <a href="https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript" target="_blank">OpenClassRooms JS beginner course</a>
in French and there is an English version available. This is rated for 15 hours
so it covers more than half of my JS course. After their part 1, you may try 
to reconnect with my JS course.

If you find the above resources difficult still, try the 
<a href="https://openclassrooms.com/fr/courses/4366701-decouvrez-le-fonctionnement-des-algorithmes" target="_blank">OpenClassRooms Introduction to Computers</a>
in French, rated for 4 hours, and there is an <a href="https://openclassrooms.com/en/courses/5261196-think-like-a-computer-the-logic-of-programming" target="_blank">American version</a>

And then you need some <a href="tools.html" target="_blank">tools</a>  on your computer to be able to follow the labs.

# Slides

### by JC Dufourd, JC Moissinac, C Concolato, P Sennelart: 02/2020

* Introduction:
  * TH1/2: <a target="_blank" href="internet.html">Internet and the Web</a>
* JavaScript:
  * TH3: <a target="_blank" href="js.html">JavaScript</a> (outside of browsers) (also see [Extras](#extras){target="_blank"})
  * TH7: <a target="_blank" href="js-browser.html">JavaScript in Browsers</a>
* Formats:
  * TH5/6:<a target="_blank" href="html.html">HTML</a>, <a target="_blank" href="css.html">CSS</a>, <a target="_blank" href="svg.html">SVG</a>
* TH9: <a target="_blank" href="technos-serveur.html">Server-side technologies</a>, <a target="_blank" href="clientserver.html">Client-server Architecture</a>
* TH11a: <a target="_blank" href="web-data.html">Processing and exchanging data on the Web</a>
* TH11b: <a target="_blank" href="web-security.html">Web and Security considerations</a>
* TH13: <a target="_blank" href="frameworks.html">Frameworks: React+Redux, Bootstrap, Angular...</a>

Access to videos with this [local playlist for French](https://peertube.r2.enst.fr/videos/watch/playlist/ee690b61-a28b-4d78-ac6e-db42b69fd4cd){target="_blank"}
and with this [other playlist for English](https://peertube.r2.enst.fr/videos/watch/playlist/0904beee-ca85-47e3-a1e8-87cb10e1bdb7){target="_blank"}

# Labs

* Extra: [Lab overview](labs-overview.html){target="_blank"}, everything you need to know about the labs to make it easier for you
* TH4: <a target="_blank" href="https://labgrader.r2.enst.fr/lab/js">JavaScript basics</a>
* TH8: <a target="_blank" href="https://labgrader.r2.enst.fr/lab/html">HTML and CSS</a>
* TH10: <a target="_blank" href="https://labgrader.r2.enst.fr/lab/js2">JavaScript AJAX</a>
* TH12: <a target="_blank" href="https://labgrader.r2.enst.fr/lab/server">Simple Server</a>
* TH14: <a target="_blank" href="https://labgrader.r2.enst.fr/lab/webapp">Web Application</a>
* TH15: <a target="_blank" href="https://labgrader.r2.enst.fr/lab/server2">REST Server</a>
* TH16: <a target="_blank" href="TP/wsdisc/wsdisc.html">Discovery of WebSockets</a>

<strong>Automatic grading</strong> of the labs is available on this
<a href="https://labgrader.r2.enst.fr/">server</a> or by using the file drop zone
at the end of each lab text.

# Extras

These are subjects that I wanted to provide deeper coverage for:

* Async: <a target="_blank" href="js-async.html">Asynchronous code in JS</a>
* Closures: <a target="_blank" href="js-closure.html">Closures</a>
* Debug: <a target="_blank" href="js-debug.html">Debugging JS</a>
* OOP: <a target="_blank" href="js-oop.html">Object oriented programming in JS</a>

The videos are part of this [local playlist in French](https://peertube.r2.enst.fr/videos/watch/playlist/ee690b61-a28b-4d78-ac6e-db42b69fd4cd){target="_blank"}
and of this [other playlist for English](https://peertube.r2.enst.fr/videos/watch/playlist/0904beee-ca85-47e3-a1e8-87cb10e1bdb7){target="_blank"}


 