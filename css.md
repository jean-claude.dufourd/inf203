# Cascading Style Sheets

<img src="css/image19.png" style="width: 40%;"/>

[pdf](css.pdf){target="_blank"}

# Concept

- Language used to associate **styles** to documents
- Companion specification to HTML
  - But can be applied to any document structured as a tree (e.g.
    HTML, XML, SVG)
- Separation CSS / HTML
  - To manage **presentation aspects** (CSS) separately from
**structural aspects** (HTML)
  - To present the content differently to different users using
different CSS
  - To present different HTML content with the same presentation
aspects, same CSS
- Demonstration
  - Deactivate CSS

# A bit of history

- CSS 1.0 (1996)
- CSS 1.0 (2nd ed., 1999)
- CSS 2.1 (2011):
  - Stable version, implemented interoperably by browsers
- CSS 3:
    - Modular specification of CSS 2.1
    - Many additions (50+ modules, see [list of
    specifications)](http://www.w3.org/Style/CSS/current-work)
    - Partly implemented by browsers

# Principles

- Language based on **rules** to be associated with document elements
- Each rule sets some **properties** on some elements
  - A rule is one or more **selectors** and a **declaration block**
    (block of properties)
- Types of properties ([more than 400
defined](https://www.chromestatus.com/metrics/css/popularity))
    - Visual properties (`background-*`, `border-*`, …)
    - Text properties (`text-*`, `font-*`, `color`, …)
    - Box properties (`padding-*`, `margin-*`, …)
    - other properties (`visibility`, `display`, `z-index`, …)
- **Style Sheet**
  - A set of rules in a separate file is a style sheet
  - Multiple style sheets can be applied to a document
    - Author style sheets
    - User style sheets
    - Device Style sheets

# Declaration of properties

- each property is declared using the syntax: property\_name + ':' +
value
```css
font-weight: 600       /* property with a unitless number value */
font-size: 16px        /* property with a number value with units */
width: 99%             /* property with a percentage value */
background-color: red  /* property with a keyword value */
font-family: 'Arial'   /* property with a string value */
background-image: url('http://my.server.com/clear.png') /* property with a complex value */
```
- use of `;` to group properties applying to the same element(s)
```css
background-color: red; font-size: 16px;
color: red;
width: 50%;
```

# CSS Units

- Size and position units
  - Absolute units
    - `px`
    - `pt`, `pc`, `cm`, `mm`, `in`: 1in = 2.54cm = 25.4mm = 72pt = 6pc
  - Relative units
    - percentage units (`%`)
    - Font-relative units: `em`,`ex`,`ch`,`rem`
    - Viewport relative units: `vw`,`vh`,`vmin`,`vmax`
- Other units
  - `deg`,`grad`,`rad`,`turn`
  - `s`,`ms`
  - `Hz`,`kHz`
  - `dpi`,`dpcm`,`dppx`

# Selectors

Select to which element(s) a block of properties apply (using ` { }`)

- Selecting elements in the document tree by tag name
```css 
p { /* these properties apply to all p elements in the page */
  border-style:solid;
  border-width:5px;
}                    
```
- Selecting using multiple tag names (separated by a comma)
```css
h1, em { /* these properties apply to all h1 and em elements in the page */
  color: blue;
}
```

# Selectors - more

- Addressing of 1 specific element in the document tree by `id`
    attribute using `#`

```html    
<!-- HTML -->
<p id="p1">text 1</p> <!-- each par has a unique id attr -->
<p id="p2">text 2</p>
```


```css
/* CSS */
#p2 { /* this prop applies to the element whose id is p2 */
    color: red;
}
#p1 { /* this prop applies to the element whose id is p1 */
    color: blue;
}
```

- Addressing of several specific elements by class name using .

```html    
<!-- HTML -->
<!-- each par has a class attr with one or more class values -->
<p class="pType1">text 1</p> 
<p class="pType1">text 2</p>
```


```css    
/* CSS */
.pType1 { /* this prop applies to all elements whose class 
             attr contains pType1 */
    color: blue;
}    
```    

# Linking CSS content with HTML content

- Via the `style` attribute (**inline stylesheet**)    
  - Styles attached to a given element (*syntax without selector*)
  ```html
  <p style="color:red;">text</p>
  ```
  - should be avoided
- Via the `style` element (**internal stylesheet**)
  - Styles attached to a given document
  ```html
  <head>
   <style>
   p { color: red; }
   </style>
  </head>
  ```
  - should be avoided
- Via an external stylesheet (separate file)    
  - Styles can be attached to a document
  ```html
  <link href="file.css" type="text/css" rel="stylesheet"/>
  ```
  - should be preferred

# CSS Cascade

- If different rules conflict (e.g. when multiple style sheets are
used)
- The rule that has precedence is determined by:
  - media type of style sheet
  - origin of rule (user agent, user, author, \!important author,
    \!important user)
  - specificity of the selector
  - order in file

![](css/cascade.jpg)

# Example of a CSS property definition

The **border-top-width** property  

Syntax: \<length\> | thin | medium | thick  
    
Definition:

|         |                          |
|----------------------|-------------------------------------------------------------------------- |
| [Initial value](/en-US/docs/CSS/initial_value){target="_blank"}   | `medium`                                                                                                                                                                                                                                                                          |
| Applies to                                       | all elements. It also applies to [`::first-letter`](/en-US/docs/Web/CSS/::first-letter "The ::first-letter CSS pseudo-element selects the first letter of the first line of a block, if it is not preceded by any other content (such as images or inline tables) on its line."){target="_blank"}. |
| [Inherited](/en-US/docs/CSS/inheritance){target="_blank"}         | no                                                                                                                                                                                                                                                                                |
| Media                                            | visual                                                                                                                                                                                                                                                                            |
| [Computed value](/en-US/docs/CSS/computed_value) | the absolute length or `0` if [`border-top-style`](/en-US/docs/Web/CSS/border-top-style "The border-top-style CSS property sets the line style of the top border of a box."){target="_blank"} is `none` or `hidden`                                                                                |
| Animatable                                       | yes, as a [length](/en-US/docs/Web/CSS/length#Interpolation "Values of the \<length\> CSS data type are interpolated as real, floating-point numbers."){target="_blank"}                                                                                                                           |

# CSS Inheritance

[![](css/CSS-inheritance.png)](css/CSS-inheritance.png)

# CSS Inheritance

- For a given element, if the value for a given property is **not specified**, the value is obtained as follows:
  - if the property is inheritable by default (i.e. "inherited: yes"),
    - if the element has a parent in the DOM tree, the **computed value** on that parent is used
```css
p { color: green }
```
``` html
<p>The text and the span will be <span>green</span> because 'color' is inheritable.</p>
```
    - otherwise (for the root), the **initial value** is used.
  - if not (i.e. "inherited: no"), the **initial value** is used
``` css
p { border-width: 1px }
```
``` html
<p>Only the text will have <span>a border</span> because 'border-width' is not inheritable.</p>
```
- The computed value is obtained:
  - by converting a relative value (when possible) to an absolute
      value
  - otherwise (% values when layout is involved), using the relative
      value

# The CSS Box Model

- Each element in the DOM produces zero, one or several boxes
depending on the type of element
  - The page rendering consists in displaying those boxes
- Each box has generic properties that controls some generic aspects:
margin, border, padding 
- The layout (size and position) of a box depends on multiple factors:
  - The size of the box and of its content (e.g. images)
  - The type of box (block, inline, ...)
  - The positioning scheme: normal, absolute, float
  - The other elements and boxes around (siblings, parent,
    containers)
  - The viewport (e.g. the window size)

![](css/image21.bmp)

# CSS Box Types

- There are 2 main types of boxes:
  - **block** boxes: Boxes that don't display on
    the same line as the previous box and as the next box
      - Sizing properties such as `width` and `height` can be used.
  - **inline** boxes: Boxes that stay on the same line as the
    previous box and the next box (when possible)
 
|     |     |
|:---:|:---:|
|![](css/image22.png)|![](css/image23.png)|
  

# CSS Box Types (continued)

- The type of box is defined by the standard:
  - block boxes: p, div, h1, h2, footer ...
  - inline boxes: a, img, span ...
- The default type can be overriden by the **display** property

```html
<p>A first par</p>
<p>A second par</p>
<a>A first link</a>
<a>A second link</a>
```

```css
p { display: inline; }
a { display: block; }
``` 

# CSS Positioning Schemes

- CSS defines the `position` property with the values
  - `static`: default value
  - `relative`: moved compared to its original position (initial
    place left empty)
  - `absolute`: positioned relative to the origin of the parent box
  - `fixed`: positioned relative to the window
- Floats

![](css/css-float.png)

- z-index

# Responsive Design

Principles

  - Design pages that adapt to the screen size using CSS Media Queries

![](css/image139.jpg)

# CSS Media Queries

- Adapt the CSS rules to apply based on client characteristics
  - Screen size, aspect-ratio, resolution or orientation
  - Type of device (pc, mobile, printer …)
  - Number of colors

```html 
<link rel="stylesheet" 
      media="screen and (max-width: 1280px)" 
      href="file.css" />                    
```

or

```html 
<link rel="stylesheet" 
      href="file-with-mediaqueries.css" />                    
```

``` 
@media screen and (max-width: 1280px)
{
    /* SomeCSS ruleshere */
}                    
```

# [Advanced Selectors](http://www.w3.org/TR/CSS/#selectors){target="_blank"}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>All elements:</td>
<td><pre style="margin: 0px;"><code>* { }</code></pre></td>
</tr>
<tr class="even">
<td>Elements with a given attribute:</td>
<td><pre style="margin: 0px;"><code>element[foo] {}</code></pre></td>
</tr>
<tr class="odd">
<td>Elements with a given attribute value:</td>
<td><pre style="margin: 0px;"><code>element[foo=‘bar’] {}</code></pre></td>
</tr>
<tr class="even">
<td>Element as a descendant of another:</td>
<td><pre style="margin: 0px;"><code>h3 em { }</code></pre></td>
</tr>
<tr class="odd">
<td>Element as a child of another:</td>
<td><pre style="margin: 0px;"><code>div &gt; p { }</code></pre></td>
</tr>
<tr class="even">
<td>Element preceded by another:</td>
<td><pre style="margin: 0px;"><code>p ~ div { }</code></pre></td>
</tr>
<tr class="odd">
<td>Pseudo-classes</td>
<td><pre style="margin: 0px;"><code>a:link {color:#FF0000;} 
a:visited {color:#00FF00;} 
a:hover {color:#FF00FF;} 
a:active {color:#0000FF;}
li:nth-child(2) {color:#0000FF;}
</code></pre></td>
</tr>
<tr class="even">
<td>Pseudo-elements</td>
<td><pre style="margin: 0px;"><code>:first-line { color: red; }
</code></pre></td>
</tr>
</tbody>
</table>

# Advanced property notation

- Short-hand notation
  - group several related properties into one
  - specific order without missing properties
```css
padding: 4px 9px;
border: 1px solid #fff;
box-shadow: inset 0 1px 2px rgba(0,0,0,.3);
```

- Vendor-prefix notation (`-o-`, `-ms-`, `-moz-`, `-webkit-`,...)

```css
-moz-box-shadow: inset 0 1px 2px rgba(0,0,0,.3);
-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.3);
```

# Authoring CSS

- Many web sites offer free CSS templates
  - <http://www.free-css.com/>
  - <http://templated.co/>
  - ...
- CSS tools
  - Pre-processors to generate CSS
      - [SASS](http://sass-lang.com/){target="_blank"}
      - [LESS](http://lesscss.org/){target="_blank"}
  - WYSIWYG editors
      - [BlueGriffon](http://bluegriffon.org){target="_blank"}
      - [SelfCSS](http://selfcss.org/){target="_blank"}
  - Responsive front-end frameworks
      - [Bootstrap](http://getbootstrap.com/){target="_blank"}
      - [Foundation](http://foundation.zurb.com/){target="_blank"}

# Summary of this lesson

* history, principle
* syntax, properties
* selectors, link with HTML
* inheritance, box model
* responsive design, media queries
* authoring