# Tools required for the INF203 labs

Here are the tools you need on your machine to follow INF203 labs:

* a browser: obviously there is already one on your machine, however I recommend using either Safari
on a Mac, or Chrome or Firefox on any machine. Edge is OK but I will have difficulties to help you
should the need arise.
* a terminal: again, it is already there but you may not have used it yet
  * on a Mac, it is called Terminal, and looks very much like a Linux terminal
  * on a Linux machine, it is called 
  * on Windows, there are two options: cmd.exe and Powershell
    * cmd.exe is the old "dos box" and has its own command syntax, use it if you are already familiar 
    with the syntax
    * PowerShell has a linux-compatible syntax, so I recommend it (of course)
* something to edit files, JS, HTML or CSS files, and you have multiple choices:
  * you could choose an IDE like Eclipse, IntelliJ or VisualCode
  * you could choose a simple text editor with syntax coloring, such as Sublime, Notepad++, Atom...
  * you could even use vi, vim, emacs, TextEdit but not Microsoft Word    
* node.js: [download from here](https://nodejs.org/en/download/){target="_blank"}

# Using a Terminal

Open a terminal.

![Terminal](images/term.png)

You see a line with the name of the machine followed by % and a blinking cursor.
In Powershell, there is the name of the current folder and > before the cursor.
This line is the "prompt".

Each Linux command has a name:

* `ls` — to list files and folders
* `pwd` — to show the location of the current folder (**p**rint **w**orking **d**irectory)
* `cd` — to **c**hange to another **d**irectory

# More terminal

Some commands require arguments. To change the current folder, you have to know, change to what ?
Arguments are added to the name of the command:

* `cd workspace` — to go to folder `workspace` (supposed to be in the current folder)
* `cd ..` — to go to the parent folder, the one containing the current folder.
    * there are two standard names :
        * `.` — is the current folder
        * `..` — is the parent folder

Try more commands :

Try ```pwd```&crarr;

If you are using a PC from Télécom, the answer is: ```/cal/homes/votrelogin```

We are using the symbol &rarr; to indicate the answer from the system, so:

Try `pwd` &crarr;

&rarr; `/cal/homes/votrelogin`.

Try ```ls```&crarr;

&rarr; The system prints the list of files and folders in the current folder.  

Play some more with cd, pwd and ls.

# A Web Server

You will also need a place with a web server, such as the personal pages of Telecom.

> Some labs will not work if you
> stay "local", i.e. if you leave all your files in
> a folder and you access them directly by "Open File"
> in the browser. 
> You need to install a web server and use a folder in its server root, for example.

Telecom Paris offers a web container accessible via the school server. 
The documentation for this service is
[there](https://eole.telecom-paris.fr/vos-services/services-numeriques/hebergement-web-persoprojetblog/guide-dutilisation-dun-container){target="_blank"}.
To activate this personal pages service, you have to go to
<Http://moncompte.telecom-paris.fr>.

However, you may also install a web server on your own machine if you choose.

