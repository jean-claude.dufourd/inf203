"use strict";

function get(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function() { cb(this.responseText); }
    xhr.send();
}

function buttonAction() { get('resource.txt', callback); }

function callback(text) {
    document.getElementById("destination").textContent = text;
}