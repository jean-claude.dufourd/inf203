<!doctype html>
<!-- copyright Institut Mines Telecom 2018 -->
<html>
<head>
    <meta charset='utf-8'>
    <title>INF203: web-security</title>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel='stylesheet' href='reveal4.5/dist/reveal.css'>
    <link rel='stylesheet' href='reveal4.5/dist/theme/sky.css' id='theme'>
    <!-- For syntax highlighting -->
    <link rel="stylesheet" href="reveal4.5/plugin/highlight/zenburn.css">
    <!-- Telecom Paris styles -->
    <link rel="stylesheet" href="slides.css">
    <!-- If the query includes 'print-pdf', include the PDF print sheet -->
    <script>
        if (window.location.search.match(/print-pdf/gi)) {
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = 'reveal4.5/css/print/pdf.css';
            document.getElementsByTagName('head')[0].appendChild(link);
        }
    </script>
</head>
<body>
<!-- Telecom Paris background -->
<div class="background">
    <div class="tpt-top-banner">
        <div class="tpt-red tpt-top-box"></div>
        <div class="tpt-black tpt-top-box"></div>
        <div class="tpt-brown tpt-top-box"></div>
    </div>
    <img class="tpt-image" src="logo-telecom.png"/>
    <div class="tpt-footer">
        <div class="tpt-red tpt-bottom-left-box"><a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"><img src="https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png"/></a></div>
        <div class="tpt-black tpt-bottom-middle-box">Jean-Claude Dufourd</div>
        <div class="tpt-brown tpt-bottom-right-box">INF203 Web Development: web-security</div>
    </div>
</div>
<!-- beginning of slides -->
<div class='reveal'>
<div class="slides">
<section id="web-security" class="slide level1">
<h1>Web Security</h1>
<p><a href="web-security.pdf" target="_blank">pdf</a></p>
<h2 id="overview">Overview</h2>
<ul>
<li>Client-side security:
<ul>
<li>risks, attacks, solutions</li>
</ul></li>
<li>Server-side security:
<ul>
<li>risks, attacks, solutions</li>
</ul></li>
</ul>
</section>
<section id="client-side-security" class="slide level1">
<h1>Client-Side Security</h1>
<ul>
<li>Anyone can create a web site, publish it on the Web and have it indexed by search engines:
<ul>
<li>including malicious persons</li>
</ul></li>
<li>Browsers execute arbitrary JavaScript code when you visit a Web site:
<ul>
<li>They don’t know a priori if a site is malicious</li>
</ul></li>
<li>Navigating to a malicious site could have more or less dramatic consequences:
<ul>
<li>Browser crash</li>
<li>Data leak (passwords, credit card information, email addresses …)</li>
<li>Identity theft (reuse of stolen information from one site to another site to send emails, transfer money…)</li>
<li>Data corruption (ransomware)</li>
<li>Illegal use of computer resources as part of botnets (spam, DDoS attacks …)</li>
</ul></li>
</ul>
</section>
<section id="browser-strategies" class="slide level1">
<h1>Browser strategies</h1>
<ul>
<li>Browsers limit client-side security issues by:
<ul>
<li>Supporting only JavaScript APIs that have been reviewed for security</li>
<li>Not allowing certain APIs (file browsing, network scanning, …)</li>
<li>Asking permissions to the user for some APIs (e.g. Camera)</li>
<li>Running the code of a web page isolated from the other pages, and from the OS:
<ul>
<li>the sand box</li>
</ul></li>
<li>Fixing bugs/vulnerabilities</li>
</ul></li>
<li>But this is not perfect:
<ul>
<li>Web users/developpers should be careful !</li>
<li>Web users/developpers update their browsers/servers regularly!</li>
</ul></li>
</ul>
</section>
<section id="same-origin-security" class="slide level1">
<h1>Same Origin Security</h1>
<ul>
<li>The architecture of the Internet is based on private sub-networks (local networks, intranets …)
<ul>
<li>It is necessary to protect a sub-network even if one computer of this sub-network is compromised</li>
<li>–&gt; Goal of the <strong>same origin policy</strong></li>
</ul></li>
<li>General principle:
<ul>
<li>A page requested from Origin A (e.g. http://www.example.org) should not be able to retrieve content from Origin B (e.g. http://mylocalserver.com)</li>
</ul></li>
</ul>
</section>
<section id="example-of-cross-origin-attack" class="slide level1">
<h1>Example of “cross-origin attack”</h1>
<p><img src="images/cross-origin-attack.png" style="width: 50%"/></p>
<ul>
<li>Example of a cross-origin attack
<ul>
<li>Computer A is in a Network X (e.g. Telecom Paris)</li>
<li>User Alice on computer A receives a link to a (malicious) Web site at http://malicious.org</li>
<li>Alice loads http://malicious.org (1).</li>
<li>http://malicious.org contains a script S to scan all resources in network X (2).</li>
<li>That script forwards all information to X (3)</li>
</ul></li>
</ul>
</section>
<section id="same-origin-policy" class="slide level1">
<h1>Same Origin Policy</h1>
<ul>
<li>Web browsers restrict resource loading based on the resource type and on the origin</li>
<li>Origin is defined as:
<ul>
<li>protocol + domain + port</li>
<li>Examples:
<ul>
<li>http://example.org is a different origin from http://example.com</li>
<li>http://www.example.org is different origin from http://data.example.org</li>
<li>http://example.org is a different origin from http://example.org:8000</li>
<li>http://example.org is a different origin from https://example.org</li>
</ul></li>
</ul></li>
<li>For historical reasons, markup resources are NOT restricted to the same origin:
<ul>
<li>HTML: an &lt;iframe&gt; can point to a different domain</li>
<li>CSS: a &lt;link&gt; element can point to a different domain</li>
<li>Images: a &lt;img&gt; element can point to a different domain
<ul>
<li>Basic web behavior: reuse an image from another site without copying</li>
</ul></li>
<li>Scripts: a &lt;script&gt; element can point to a different domain
<ul>
<li>Important for CDN hosted scripts (e.g. jQuery, …)</li>
</ul></li>
<li>Video: a &lt;video&gt; element can point to a different domain</li>
</ul></li>
</ul>
</section>
<section id="same-origin-and-javascript" class="slide level1">
<h1>Same Origin and JavaScript</h1>
<ul>
<li>The main restriction is on JavaScript downloads:
<ul>
<li>XMLHTTPRequests calls in AJAX are restricted to the Same Origin</li>
<li>Unless
<ul>
<li>The request is “simple”</li>
<li>CORS is used</li>
</ul></li>
<li>Some work-around exists:
<ul>
<li>create a script tag to make the download (JSONP)</li>
<li>Use a proxy so that all requests come from the same origin</li>
</ul></li>
</ul></li>
<li>JavaScript Communication between pages of different origins is possible using JavaScript postMessage API</li>
</ul>
</section>
<section id="cors" class="slide level1">
<h1>CORS</h1>
<h2 id="http-cross-origin-resource-sharing">HTTP Cross-Origin Resource Sharing</h2>
<ul>
<li>Server A explicitly allows clients to use its data within JavaScript if downloaded from server B</li>
</ul>
<p><a href="images/cors-archi.png"><img data-src="images/cors-archi.png" /></a><br />
<a href="images/cors-request.png"><img data-src="images/cors-request.png" /></a> <a href="images/cors-response.png"><img data-src="images/cors-response.png" /></a></p>
</section>
<section id="cookies-and-cross-origin" class="slide level1">
<h1>Cookies and Cross-origin</h1>
<ul>
<li>Reminder: Cookies are small text files, containing server information, stored on the client-side and exchanged in HTTP or HTTPS requests
<ul>
<li>Mainly used today as tracking tools for advertizers (annoying) but server-side tools can do worse…</li>
<li>Can be used to keep track that the user is logged (cookie is set by the server after login/password have been verified)</li>
<li>Cookies can be risky if used to store passwords or long-lived client session information. Modern web sites use short-lived, randomized session ids</li>
<li>Cookie can be session-based or permanent (“remember my identifier”)</li>
<li>Cookies follow the same origin principle but with a looser concept of Origin</li>
<li>Can be created/manipulated in JavaScript using <code>document.cookie</code>. Be careful when loading cross origin scripts, they may steal cookies (unless HttpOnly is used)</li>
</ul></li>
</ul>
</section>
<section id="some-client-side-attacks" class="slide level1">
<h1>Some client-side attacks</h1>
<ul>
<li>Clickjacking</li>
<li>Phishing</li>
<li>Data Sniffing</li>
</ul>
</section>
<section id="clickjacking" class="slide level1">
<h1>Clickjacking</h1>
<ul>
<li>Problem:
<ul>
<li>The user is made to be believe that they click on something (e.g. “click here to get a free gift”), while clicking on something else (e.g. allowing camera access, …).</li>
<li>Usually done using transparent &lt;iframe&gt;</li>
</ul></li>
<li>Solution:
<ul>
<li>Limit the use of &lt;iframe&gt; with <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options" target="_blank"><code>X-Frame-Options</code></a>: disallowed or same origin</li>
<li>Make sure in JavaScript that the click happend on the most top level window</li>
</ul></li>
</ul>
</section>
<section id="phishing" class="slide level1">
<h1>Phishing</h1>
<ul>
<li>Problem
<ul>
<li>Alice receives of a link (via email, messaging, …) to a site that looks like another site (e-commerce, bank …)</li>
<li>Alice believes that she is on the official site and enters her personal information (login, password, credit card …)</li>
<li>Her personal information is stolen and used elsewhere.</li>
</ul></li>
<li>Solution:
<ul>
<li>As a user, make sure the URL of a site is the right one.</li>
<li>As a developer, require HTTPS for sensitive sites</li>
</ul></li>
</ul>
</section>
<section id="data-sniffing" class="slide level1">
<h1>Data Sniffing</h1>
<ul>
<li>Problem:
<ul>
<li>On a wired networks (including possibly within operator’s networks) or on a WiFi network not sufficiently protected (e.g. WEP), one can capture IP packets and analyze them.</li>
<li>HTTP packets may contain login, password, credit card information that anyone can copy</li>
<li>By scanning packets on a network, anyone can steal information</li>
</ul></li>
<li>Solution:
<ul>
<li>As a user: do not transmit sensitive information unless the site uses HTTPS</li>
<li>As a developper: do not setup sites requiring sensitive information with HTTP, require HTTPS</li>
</ul></li>
</ul>
</section>
<section id="server-side-security" class="slide level1">
<h1>Server-side Security</h1>
<ul>
<li>The Web and the Internet are <strong>hostile</strong> environments!</li>
<li>Unless the access to the server is restricted (firewalls, NATs …), anyone from the Internet can make request to a web server, including <strong>malicious requests</strong></li>
<li>Web security is a <strong>shared</strong> responsibility: web server administrator and web master</li>
</ul>
</section>
<section id="server-side-security-risks" class="slide level1">
<h1>Server-side Security Risks</h1>
<p>Data leak (e.g. Yahoo!, Ashley Madison …)</p>
<p>Data ransom</p>
<p>Illegal use of server resources as part of a botnet (storage, spam or DDoS)</p>
</section>
<section id="server-side-security-measures-why" class="slide level1">
<h1>Server-side Security Measures: Why?</h1>
<ul>
<li>Some verifications of the data sent to the server can be made at the client-side:
<ul>
<li>Limit text field size, file upload size, …</li>
<li>use radio buttons or pre-determined choices to avoid arbitrary text upload</li>
<li>Use JavaScript client-side checks to verify the validity of data sent</li>
</ul></li>
<li>But a malicious person will probably not use your site to attack the server
<ul>
<li>Necessity to perform input checks at server-side to avoid “code injection”</li>
</ul></li>
</ul>
</section>
<section id="xss---cross-site-scripting-problem" class="slide level1">
<h1>XSS - Cross Site Scripting: Problem</h1>
<ul>
<li>A Web Site contains a form to send text content to a server</li>
</ul>
<pre><code>&lt;form action=&quot;http://myserver.com/hello&quot;&gt;
&lt;input type=&quot;text&quot; name=&quot;Nom&quot;&gt;
&lt;/form&gt;</code></pre>
<ul>
<li>A malicious user uses this site to send HTML content including &lt;script&gt; (not just text) to the server</li>
</ul>
<pre><code>World &lt;script&gt;alert(&#39;You have a won the lottery!&#39;)&lt;/script&gt;                                     </code></pre>
<ul>
<li>The server receives this content, treats it as simple text and returns an HTML response based on this text content</li>
</ul>
<pre><code>return &quot;&lt;html&gt;&lt;body&gt;&lt;p&gt;Hello&quot; + Nom + &quot;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;&quot;</code></pre>
<ul>
<li>The browser receives the new HTML page and execute the malicious-user-inserted HTML:
<ul>
<li>A pop-up appears that was not planned by the Web Site author!</li>
<li>Any arbitrary code could be run (leaking cookies, …)</li>
<li>It is even worse if the injected HTML is saved and sent to every new user afterwards (e.g. comment section in a forum)</li>
</ul></li>
</ul>
</section>
<section id="xss-solutions" class="slide level1">
<h1>XSS: Solutions</h1>
<ul>
<li>Check where the text content will be inserted</li>
<li>Never insert untrusted text content except in allowed locations (e.g. NOT in scripts, NOT as attribute names, or NOT as element names, or NOT as comments, or NOT in CSS content)</li>
<li>Use specific escaping mechanisms depending on where the text content is inserted
<ul>
<li>Escape HTML special characters when saving/returning text content in HTML text content using HTML entities (<code>&amp;...;</code>)
<ul>
<li>If a malicious user enters <code>&lt;script&gt;</code> the server should use <code>&amp;lt;script&amp;gt;</code></li>
</ul></li>
<li>In many server-side programming languages, there are helper functions. In PHP, you can use the function <code>htmlspecialchars</code>, <code>htmlentities</code>, or <code>strip_tags</code> In NodeJS or Python, use HTML escape modules</li>
<li>Use specific escape mechanism when inserting text in attribute content (e.g. quote)</li>
<li>Use specific escape mechanism when inserting text in script tags (if allowed at all!)</li>
</ul></li>
<li>See <a href="https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet">Cheat Sheet</a> for detailed rules</li>
</ul>
</section>
<section id="xsrf---cross-site-request-forgery" class="slide level1">
<h1>XSRF - Cross Site Request Forgery</h1>
<ul>
<li>Problem:
<ul>
<li>User Alice is already logged onto a site (Forum, Blog, …)</li>
<li>Malicious user Bob sends a link to a malicious page</li>
<li>The malicious page contains a ‘hidden’ link, such as:<br />
</li>
</ul>
<pre class="html"><code>&lt;img src=&quot;http://www.example.com/forum/delete_user.php?user=Donald&quot;/&gt;                        </code></pre>
<ul>
<li>Alice erroneously clicks on the link without realizing, because Alice is still logged in:
<ul>
<li>User Donald is removed from the forum</li>
</ul></li>
</ul></li>
<li>Solutions:
<ul>
<li>Use HTTP POST requests to make it more complex to send the parameters and to force a page refresh when the action is done (make the user aware)
<ul>
<li>Can be worked-around with 0-width 0-height iframes</li>
</ul></li>
<li>Limit the use of <code>&lt;iframe&gt;</code> with <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options" target="_blank"><code>X-Frame-Options</code></a>: disallowed or same origin</li>
<li>Check the referer: where this action comes from</li>
<li>Best practice: Use random tokens to avoid the URL of the action to be known in advance</li>
</ul></li>
</ul>
</section>
<section id="sql-injection" class="slide level1">
<h1>SQL injection</h1>
<ul>
<li>Problem
<ul>
<li>If the user input text is used to be build SQL requests, well-crafted text can leak database information</li>
<li>Example:
<ul>
<li>The HTML form includes a text field called “passwd”<br />
</li>
<li>The server uses this text to make a request:<br />
</li>
</ul>
<pre><code>mysql_query(&quot;SELECT * FROM T WHERE passwd=&#39;$passwd&#39;&quot;)</code></pre>
<ul>
<li>If the input passwd text is: <code>' OR 1=1 --</code> the request becomes:</li>
</ul>
<pre><code>mysql_query(&quot;SELECT * FROM T WHERE passwd=&#39;&#39; OR 1=1 --&#39;&quot;)</code></pre></li>
<li>Which returns all values !!</li>
</ul></li>
<li>Solution
<ul>
<li>Do not construct SQL requests from user text inputs</li>
<li>If not possible, escape text content (e.g. PHP <a href="http://php.net/manual/fr/function.mysql-real-escape-string.php" target="_blank"><code>mysql_real_escape_string</code></a>)</li>
</ul></li>
</ul>
</section>
<section id="command-line-injection" class="slide level1">
<h1>Command-line injection</h1>
<ul>
<li>Problem
<ul>
<li>If the user input text is used to be call executables (e.g. using <code>exec</code> in PHP or NodeJS), well-crafted text can execute arbitrary code</li>
<li>Example:
<ul>
<li>The HTML form includes a text field called “a”</li>
<li>The server uses this text to list the content of a directory named “a”:</li>
</ul>
<pre><code>exec(&quot;ls $a&quot;)</code></pre>
<ul>
<li>If the input text is: <code>&amp;&amp; cat /etc/passwd</code> the system call becomes:</li>
</ul>
<pre><code>exec(&quot;ls &amp;&amp; cat /etc/passwd&quot;)</code></pre></li>
<li>Passwords are leaked!!</li>
</ul></li>
<li>Solution
<ul>
<li>Avoid calling executables using parameters input from a web page</li>
<li>If not possible, inspect final command line and escape text content (e.g. PHP <a href="http://php.net/manual/fr/function.escapeshellcmd.php" target="_blank"><code>escapeshellcmd</code></a> or <a href="http://php.net/manual/fr/function.escapeshellarg.php" target="_blank"><code>escapeshellarg</code></a>)</li>
</ul></li>
</ul>
</section>
<section id="directory-traversal" class="slide level1">
<h1>Directory Traversal</h1>
<ul>
<li>Problem
<ul>
<li>If the user input text is used to open files, well-crafted text (i.e. using ‘..’ and ‘/’) can allow reading any file</li>
<li>Example:
<ul>
<li>The HTML form includes a text field called “file”</li>
<li>The server uses this text to list open a file and return its content:</li>
</ul>
<pre><code>read($file) </code></pre>
<ul>
<li>If the input text contains: <code>../../../../../etc/passwd</code>, passwords are leaked!!</li>
</ul></li>
</ul></li>
<li>Solution
<ul>
<li>Avoid allowing reading of a file whose name is based on an input from a web page</li>
<li>If not possible, inspect the final path and disallow specific paths</li>
</ul></li>
</ul>
</section>
<section id="summary-of-the-lesson-web-data-and-web-security" class="slide level1">
<h1>Summary of the lesson (web data and web security)</h1>
<ul>
<li>Web data, text, internationalization, text rendering</li>
<li>Character set, Unicode, encoding, UTF-8</li>
<li>Structured text, CSV, XML, JSON, JSONP</li>
<li>REST and web APIs</li>
<li>Security, browser protection</li>
<li>Same origin, cross origin, CORS</li>
<li>Client-side attacks: clickjacking, phishing, data sniffing</li>
<li>Server attacks: injections</li>
</ul>
</section>
</div>
</div>
<script src="reveal4.5/dist/reveal.js"></script>
<script src="reveal4.5/plugin/notes/notes.js"></script>
<script src="reveal4.5/plugin/markdown/markdown.js"></script>
<script src="reveal4.5/plugin/highlight/highlight.js"></script>
<script>
    // More info about initialization & config:
    // - https://revealjs.com/initialization/
    // - https://revealjs.com/config/
    Reveal.initialize({
        hash: true,
        slideNumber: true,
        // Learn about plugins: https://revealjs.com/plugins/
        plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ]
    });
</script>
</body>
</html>
