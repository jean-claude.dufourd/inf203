# AJAX for designers

This is a step-by-step proof that AJAX is simple to understand.

This starts with a simple text resource on the web. We are going to access the resource in different ways.
The resource itself is a file named resource.txt present in the very same folder as the current HTML.

All 4 ways are similar: the resource is loaded with HTTP at different times.

* Way 1: access when clicking on a link.
* Way 2: access when showing an element.
* Way 3: access when clicking on button (with AJAX).
* Way 4: same as way 3 plus interpretation of the resource after load.

Way 1 is a simple [link](resource.txt){target="_blank"} to the resource. 

Way 2 is the same resource loaded into an iframe:

<iframe src="resource.txt" style="width: 700px; height: 150px; font-family: Arial,sans-serif; font-size: 24px;text-align: left;"></iframe>

```html
<iframe src="resource.txt" style="..."></iframe>
```

# The core AJAX function

Here is the core AJAX function:

```javascript
function get(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function() { cb(this.responseText); }
    xhr.send();
}
```

You do not need to understand the ***get*** function, just that its parameters are a ***url*** of a resource, 
and a callback function named ***cb***. The callback function is called with the loaded text as parameter, when the text
has arrived. Next time, just copy-paste this ***get*** function if needed.

# Way 3: Minimal AJAX

Here is the source code of a minimal AJAX script:

```javascript
// we reuse the get function from ajax.js
function buttonAction() { // for button onclick
    get('resource.txt', callback); 
}

function callback(text) {
    document.getElementById("destination").textContent 
        = text;
}
```

The ***callback*** function adds 
the received text to a ***div***. There is no interpretation: the text is seen as plain text.
The ***buttonAction*** invokes ***get*** on the resource url and the callback.


# Way 3: Minimal AJAX in action

The HTML contains:

```html
<script src="ajax.js"></script>
<button type="button" onclick="buttonAction()">Make Minimal Request</button>
<div id="destination" style="..."></div>
```

<script src="ajax.js"></script>

Click on the button below to make the request and see the result:

<button type="button" onclick="buttonAction()">Make Minimal Request</button>

<div id="destination" style="background-color: #4BB1ff; width: 700px; height: 300px;text-align: left; font-family: Arial,sans-serif; font-size: 24px; "></div>

In Way 3, the resource is loaded when the button is clicked.

# Way 4: Full AJAX

Here another callback processes the resource before adding it:

```javascript
// we reuse the get function
function buttonAction2() { // for button onclick
    get('resource.txt', callback2); 
}

function callback2(text) {
    let json = JSON.parse(text);
    document.getElementById("destination2").textContent 
        = json.text;
    document.getElementById("destination3").innerHTML
        = json.html;
}
```
The ***callback2*** function receives the text of the resource, translates it from JSON text to a JS object, then puts the
value of the ***text*** property in the first ***div*** below and puts the value of the ***html*** property in the second
***div*** below.

In Way 4, the resource is also loaded when the button is clicked: the difference is that the resource is then
interpreted piece by piece to construct HTML.

# Full AJAX in action

The HTML contains:

````html
<script src="ajax2.js"></script>
<button type="button" onclick="buttonAction2()">Make Request</button>
<div id="destination2" style="..."></div>
<div id="destination3" style="..."></div>
````

<script src="ajax2.js"></script>

Click on the button below to make the request and see the result:

<button type="button" onclick="buttonAction2()">Make Request</button>

<div id="destination2" style="background-color: #4BB1ff; width: 700px; height: 200px;text-align: left; font-family: Arial,sans-serif; font-size: 18px; "></div>
<div id="destination3" style="background-color: lightblue; width: 700px; height: 100px;text-align: left; font-family: Arial,sans-serif; font-size: 18px; "></div>

# Conclusion

The AJAX core function is 4 lines long, and you do not need to understand it in detail nor remember it. 
Just copy-paste it when you need to use it.

The JS code you have to write is 6 to 10 lines, the HTML is 3-4 lines.

With AJAX, the received resource is processed with JS+DOM, same as you are doing in JS animations.

In this simple case, the resource is a simple constant file. In more complex cases, the resource could be constructed 
dynamically on the server: it could be the latest news from AFP, the content of your Inbox...

