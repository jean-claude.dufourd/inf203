%.pdf: %.md preamble.tex
	pandoc $< -o $@ -t beamer -H preamble.tex --highlight-style=zenburn


%.html: %.md template.html
	pandoc $< -o $@ -t revealjs --template=template.html --no-highlight --metadata=title:$*


all: *.html *.pdf

html: *.html

pdf: *.pdf
