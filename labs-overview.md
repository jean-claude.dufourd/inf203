# Labs overview

This extra is about how to manage Labs. This extra contains:

- information about the tools that you should use
- information about the different situations you are going to work in
- for each situation, how you should go about doing the work (creation, test, debug)

# Situations

1. writing JS only
1. writing JS for use within an HTML page shown in a browser
1. writing a JS webserver
1. writing a web application (both client and server in JS)

# One: JS Only

The JS lab is about designing pure JS code, with no connection to anything else.

You write JS in a test file with extension .js in your favorite text editor: could be vi, vim, emacs, sublime, notepad++, eclipse, intellij...

You execute the .js file by loading it in `node.js`. `node.js` is installed on the machines at Télécom. 
You have to install it on your machine. It is available on Windows, MacOS, Linux...

You debug your creation:
- in eclipse, same way you debugged a Java program in INF103
- in intellij
- in a Chrome/Chromium browser: see [this page](https://nodejs.org/en/docs/guides/debugging-getting-started/){target="_blank"} to be 
  able to debug JS executing in node.js as if it was executing in the browser.

You may also add console.log instructions to debug your programs with prints.

>You need to test each function you wrote by calling them on at least two or three values and checking the validity of the
result.

# One': MJS ?

Now the JS lab has been switched to "MJS" or the ES6 syntax.

Old files are with extension .js and use `require("module")`. These files are
CommonJS files. Any .js file is assumed to be CommonJS.

New files are with extension .mjs and use `ìmport {func} from "module"`.
These files are using ES6 syntax. Files with .mjs extension are assumed to be ES6.

Using require in .mjs or import in .js will give an error.
Mixing import and require will give an error.

Each type can call a module of the other type, even if they cannot be 
mixed within one file.

# Two: JS in the browser

JS in the browser is similar to JS Only with all the resources of a browser. 

JS in the browser has to be loaded by an HTML file. Either the JS code is inside the `script` tag of the HTML, 
or it is in a separate file, and referred, inside the HTML, by the `src` attribute of the `script` element.

JS in the browser is very limited if you load it from a file. Many things, including AJAX, are forbidden if the JS 
is loaded from a file.

You have to load a JS program (HTML and JS files) from a webserver to enable its full potential.
A JS file loaded from the file system in the browser will not be allowed to execute AJAX requests.

References in HTML depend on the module syntax (CommonJS or ES6):

```
<script type="application/ecmascript" src="foo.js"/>

<script type="module" src="bar.mjs"/>
```


# Two: Using an existing webserver to serve your HTML and JS

Telecom Paris offers a web container accessible via the school server.
The documentation for this service is
[there](https://eole.telecom-paris.fr/vos-services/services-numeriques/hebergement-web-persoprojetblog/guide-dutilisation-dun-container){target="_blank"}.
To activate this personal pages service, you have to go to
<http://moncompte.telecom-paris.fr>.

You may also use a webserver installed on your laptop or home desktop. In that case, you need to place your files in the
folder (or one of the folders) shared by this webserver. Read the documentation of the installed webserver to know where
to put the files to be served.

# Two: Debugging JS in the browser

Debugging JS in the browser starts with opening the `inspector`. Right-click in the browser and choose "Inspect" or 
"Inspect Element". Then the browser window is split in two, and one part is dedicated to debugging. The elements of the
debugging window are:

- Elements/Inspector tab
- Sources/Debugger tab
- Console tab
- Network tab

The Elements/Inspector tab shows you the DOM tree (i.e. the HTML elements and the CSS styles), allows ou to do editing

The Sources/Debugger tab shows you all the source of files used in the current site, including JS files where you can
place break points, you can run/step over/step into/step out just like in Eclipse.

The Console tab allows you to experiment, evaluate expressions, see error messages, expand complex variables...

The Network tab shows you, for requests sent after the tab is opened, everything about the HTTP requests and responses, 
headers and content.

# Two: Testing JS in the browser

You need to test every button and every action of your system.

To check the validity of the result, you need to see the correct result in the main window, and you need to open the
inspector window and check the resulting HTML: some mistakes can only be seen in the inspector.

# Three: JS server

Writing a JS webserver starts like `One: JS Only` but is tested differently.
You write a .js or .mjs file, which you execute in node.js. 
However, the server will do nothing until you send HTTP requests to it.

So if your file is called `server.js`, you run your server with `node server.js 8000` (8000 is the port to use)

Then you have a choice of tools to send HTTP requests:

- curl: see below
- your browser: just for standard GET requests

>You need to test your server with all the URLs that it should recognize, plus a few that it should not recognize, to
test the error recovery of your server.

# Three: How to test a JS server

Open two terminal windows.

In the first terminal window, start the server. It will run and if there are errors, you will see the informations
printed on the terminal.

In the second terminal window, send with curl, one by one, all the URLs that your server should know how to respond 
to, plus a few others to test what your server does in case of errors. With the options of curl, you can select to
see just the content, or juste the response headers, or both.

# Four: Web Application

A Web application is the combination of Two: and Three: a web server written by yourself and JS running in the browser.

Whenever there is a bug, it could be in the server or in the client. So you need to test both sides separately.

I suggest you start testing the server, with all the URLs that the client might be sending. See previous slide.

When you are certain the server works according to the lab text, test the client with the server.
In that case, if you find a bug now, the principal suspect is the client.

Use the Network tab of the browser inspector to understand which request is incorrect, or which response is 
misunderstood, keeping in mind that maybe you did not test the server completely.

# Tools

1. curl
2. http-echo-server
3. Network tab in browser inspector

# curl, debugging servers

`curl` is a command line tool that exists on Windows, MacOS and Linux, I think by default. 
It allows you to send HTTP requests to test a server that you are writing.

`curl http://localhost:3000/` sends the URL with a GET request, and prints the resulting content.

`curl -i http://localhost:3000/` sends the URL with a GET request, and prints the response (with headers) and the resulting content.

`curl -I http://localhost:3000/` sends the URL with a GET request, and prints the response with the headers only (no content).

`curl -d '{"key":"imaginary"}' -H "Content-Type: application/json" -X POST http://localhost:8005/publication` sends the 
URL with a POST request, some data and extra headers:

- `-d` specifies the data to upload
- `-H` specifies the additional HTTP headers to send
- `-X` specifies the HTTP method, by default GET

The manual page of `curl` is [there](https://curl.se/docs/manpage.html){target="_blank"}

# http-echo-server, debugging clients (Java, python, Android)

In a situation where you are creating an HTTP request, either in the browser or in node.js, and you want to test it, 
you can use the http-echo-server. This is a JS program running in node.js and which dumps on the console everything
it received:

- the request
- all the headers
- if relevant, the received content

To install it, on a machine with node.js, run `npm install http-eco-server -g` (with admin rights).
Then run `http-echo-server 3000` where 3000 is the port number you want to listen.

# http-echo-server output

Sample output for `curl -X DELETE http://localhost:3000/`:

```
jcdufourd@Mac test % http-echo-server 3000
[server] event: listening (port: 3000)
[server] event: connection (socket#1)
[socket#1] event: resume
[socket#1] event: data
--> DELETE / HTTP/1.1
--> Host: localhost:3000
--> User-Agent: curl/7.64.1
--> Accept: */*
--> 
--> 
[socket#1] event: prefinish
[socket#1] event: finish
[socket#1] event: readable
[socket#1] event: end
[socket#1] event: close
```

# Browser/Inspect/Network, debugging JS code sending or responding to HTTP requests

In a situation where you are creating an HTTP request in the browser, and you want to test it, or want to see the
details of the server response to your request, you can use the Network tab of the browser's inspector.
The network tab needs to be open before you make or receive the request.
And you can only send GET requests easily from the browser (unless you load some extension like Postman).

If the first request triggers others, you will see them all in the time line.

When you click on the request that you want to explore in detail, you have a choice of sub-tabs:

- Headers: all headers sent and received
- Response
- detailed timing
- Cookies

