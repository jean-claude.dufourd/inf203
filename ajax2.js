"use strict";

// we reuse the get function from ajax.js

function buttonAction2() { get('resource.txt', callback2); }

function callback2(text) {
    let json = JSON.parse(text);
    document.getElementById("destination2").textContent = json.text;
    document.getElementById("destination3").innerHTML = json.html;
}