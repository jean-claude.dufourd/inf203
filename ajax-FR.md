# AJAX pour designers

Ceci est une preuve qu'AJAX est simple à comprendre

Je vous propose de manipuler une simple ressource texte sur le web, de plusieurs manières dont certaines déjà connues.
La ressource est un fichier texte présent dans le dossier à coté du fichier HTML.

Les 4 versions se ressemblent: la ressource est chargée avec HTTP à différents moments.

* Version 1: chargement quand vous cliquez sur un lien.
* Version 2: chargement quand vous montrez un élément (ici, au chargement de la page).
* Version 3: chargement quand vous cliquez sur un bouton (avec AJAX).
* Version 4: comme version 3 plus interpretation de ce qui a été reçu.

Version 1 est un simple [lien](resource.txt){target="_blank"} vers la ressource. 

Version 2 montre la même ressource chargée dans un iframe:

<iframe src="resource.txt" style="width: 700px; height: 150px; font-family: Arial,sans-serif; font-size: 24px;text-align: left;"></iframe>

```html
<iframe src="resource.txt" style="..."></iframe>
```

# La fonction AJAX centrale

Voici la fonction AJAX centrale:

```javascript
function get(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function() { cb(this.responseText); }
    xhr.send();
}
```

Vous n'avez pas besoin de comprendre le fonctionnement de la fonction ***get***.
Vous devez juste savoir que ses paramêtres sont l'***url*** d'une ressource, 
et une fonction 'callback' appelée ***cb***. 

La function 'callback' est appelée ainsi parce que c'est la fonction qui est appelée
quand le texte a été chargé, et le texte est fourni en paramêtre d'entrée de la fonction.

La prochaine fois que vous avez besoin d'AJAX, faites juste un copier coller de la fonction ***get***.

# Version 3: AJAX de base

Voici le code source d'un script minimal invoquant AJAX:

```javascript
function buttonAction() { // for button onclick
    get('resource.txt', callback); 
}

function callback(text) {
    document.getElementById("destination").textContent 
        = text;
}
```

La fonction ***callback*** ajoute le texte reçu à un ***div***. 
Il n'y a pas d'interprétation du texte, il est vu comme du texte simple.
La fonction ***buttonAction*** appelle la fonction ***get*** sur l'url de la ressource et la fonction callback.


# Version 3: AJAX de base en action

Le fichier HTML doit contenir:

```html
<script src="ajax.js"></script>
<button type="button" onclick="buttonAction()">Make Minimal Request</button>
<div id="destination" style="..."></div>
```

<script src="ajax.js"></script>

Cliquez sur le bouton ci-dessous pour déclencher le chargement et voir le résultat:

<button type="button" onclick="buttonAction()">Make Minimal Request</button>

<div id="destination" style="background-color: #4BB1ff; width: 700px; height: 300px;text-align: left; font-family: Arial,sans-serif; font-size: 24px; "></div>

Dans la version 3, la ressource est chargée après le clic sur le bouton.

# Version 4: AJAX plus évolué

Voici un exemple où la ressource est interprétée après chargement:

```javascript
function buttonAction2() { // for button onclick
    get('resource.txt', callback2); 
}

function callback2(text) {
    let json = JSON.parse(text);
    document.getElementById("destination2").textContent 
        = json.text;
    document.getElementById("destination3").innerHTML
        = json.html;
}
```

La fonction ***callback2*** reçoit le texte de la ressource, traduit ce texte qui est un JSON en objet JavaScript,
puis place la valeur de la propriété ***text*** du résultat dans le premier ***div*** ci-dessous
et place la valeur de la propriété ***html*** dans le second ***div*** ci-dessous.

Dans cette version 4, la ressource est aussi chargée après clic sur le bouton: 
la différence est dans le code qui interpréte le texte, morceau par morceau, pour construire le HTML.

# AJAX plus évolué en action

Le fichier HTML doit contenir:

````html
<script src="ajax2.js"></script>
<button type="button" onclick="buttonAction2()">Make Request</button>
<div id="destination2" style="..."></div>
<div id="destination3" style="..."></div>
````

<script src="ajax2.js"></script>

Cliquez sur le bouton ci-dessous pour déclencher le chargement et voir le résultat:

<button type="button" onclick="buttonAction2()">Make Request</button>

<div id="destination2" style="background-color: #4BB1ff; width: 700px; height: 200px;text-align: left; font-family: Arial,sans-serif; font-size: 18px; "></div>
<div id="destination3" style="background-color: lightblue; width: 700px; height: 100px;text-align: left; font-family: Arial,sans-serif; font-size: 18px; "></div>

# Conclusion

La fonction centrale d'AJAX fait 4 lignes, et vous n'avez pas besoin de comprendre ses détails, ni de vous en souvenir.
Faites un copier coller quand vous en avez besoin. C'est ce que j'ai fait pendant des années.

Le code JS pour utiliser AJAX fait entre 6 et 10 lignes, le HTML 3 à 4 lignes.

Avec AJAX, le texte chargé est manipulé par du code JS avec l'API DOM, ce que vous utilisez déjà 
pour faire des animations en JS.

Dans ce code, la ressource est un simple fichier existant. Dans des cas plus complexes, la ressource
pourrait être générée à la demande sur le serveur: les dernieres nouvelles de l'AFP, le contenu de votre boite mail...

