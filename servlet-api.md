# Servlet API

just for information

[pdf](servlet-api.pdf){target="_blank"}


# HttpServlet API

  - protected void doGet(HttpServletRequest req, HttpServletResponse
    resp)
  - throws ServletException, IOException;
  - protected void doHead …
  - protected void doPost …
  - protected void doPut …
  - protected void doDelete …
  - protected void doOptions …
  - protected void doTrace …

# ServletRequest API

  - public Object getAttribute(String name);
  - public Enumeration getAttributeNames();
  - public String getCharacterEncoding();
  - public int getContentLength();
  - public String getContentType();
  - public String getParameter(String name);
  - public Enumeration getParameterNames();
  - public String\[\] getParameterValues(String name);
  - public String getProtocol();
  - public String getScheme();
  - public String getServerName();
  - public int getServerPort();
  - public ServletInputStream getInputStream() throws IOException;
  - public BufferedReader getReader () throws IOException;
  - public String getRemoteAddr();
  - public String getRemoteHost();
  - public void setAttribute(String key, Object o);
  - public String getRealPath(String path);

# HttpServletRequest API

  - public String getAuthType();
  - public Cookie\[\] getCookies();
  - public HttpSession getSession (boolean create);
  - public HttpSession getSession();
  - public long getDateHeader(String name);
  - public String getHeader(String name);
  - public Enumeration getHeaderNames();
  - public int getIntHeader(String name);
  - public String getMethod();
  - public String getPathInfo();
  - public String getPathTranslated();
  - public String getQueryString();
  - public String getRemoteUser();
  - public String getRequestedSessionId ();
  - public String getRequestURI();
  - public String getServletPath();
  - public boolean isRequestedSessionIdValid ();
  - public boolean isRequestedSessionIdFromCookie ();
  - public boolean isRequestedSessionIdFromURL();
  - public boolean isRequestedSessionIdFromUrl ();

# ServletResponse API

  - public String getCharacterEncoding ();
  - public ServletOutputStream getOutputStream() throws IOException;
  - public PrintWriter getWriter() throws IOException;
  - public void setContentLength(int len);
  - public void setContentType(String type);

# HttpServletResponse API

  - public void addCookie(Cookie cookie);
  - public boolean containsHeader(String name);
  - public String encodeURL (String url);
  - public String encodeRedirectURL (String url);
  - public String encodeUrl(String url);
  - public String encodeRedirectUrl(String url);
  - public void sendError(int sc, String msg) throws IOException;
  - public void sendError(int sc) throws IOException;
  - public void sendRedirect(String location) throws IOException;
  - public void setDateHeader(String name, long date);
  - public void setHeader(String name, String value);
  - public void setIntHeader(String name, int value);
  - public void setStatus(int sc);
  - public void setStatus(int sc, String sm);
  - \+ codes d’erreur

# Customer state

  - A session is: HTTPSession
  - public long getCreationTime ();
  - public String getId ();
  - public long getLastAccessedTime ();
  - public Object getValue (String name);
  - public String \[\] getValueNames();
  - public void invalidate ();
  - public boolean isNew ();
  - public void putValue (String name, Object value);
  - public void removeValue (String name);
  - public void setMaxInactiveInterval(int interval);

# Cookies

  - String name; // NAME= ... "$Name" style is reserved
  - String value; // value of NAME
  - String comment;// ;Comment=VALUE ... describes cookie's use
  - String domain; // ;Domain=VALUE ... domain that sees cookie
  - int maxAge=-1; // ;Max-Age=VALUE ... cookies auto-expire
  - String path; // ;Path=VALUE ... URLs that see the cookie
  - boolean secure;// ;Secure ... e.g. use SSL
  - int version=0; // ;Version=1 ... means RFC 2109++ style

