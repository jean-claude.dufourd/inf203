# HyperText Markup Language

<img src="html/html5-logo.png" style="width: 50%;"/>

[pdf](html.pdf){target="_blank"}

# HTML: A bit of history

[![](html/html_versions.png)](html/html_versions.png)

- Initial version created by Tim Berners-Lee in 1989
  - as an open language, royalty-free
  - Then developed by the World Wide Web Consortium (W3C)
  - Now developed by W3C and WHATWG
- Several versions
  - HTML 4 Strict, Transitional, Frameset
  - HTML vs. XHTML
  - HTML 5 (HTML.next, [HTML 5.1](https://www.w3.org/TR/html51/){target="_blank"})

# HTML 5:  The language

- 1 language, 2 syntaxes
  - **HTML**, identified by documents of type `text/html`
  - **XHTML** (XML), identified by `application/xhtml+xml`
  - Similar syntaxes but different processing (e.g. +/- strict)
- Text-based
  - mix of **tags** (markup) and text
  - no compilation step
  - can easily view the source code
- Presentation agnostic
  - Might be rendered by different renderers (screen, printer,
    text-only, speech, ...)
  - Rendering can be configured via **CSS**
- Basic Interactivity (navigation, forms)
  - Advanced Interactivity to be provided by **JS**
- Associated with a tree representation and JS APIs: **DOM**

# HTML 5: by example

Go into [https://jsfiddle.net/](https://jsfiddle.net/){target="_blank"}:

* experiment with tags html, head, body, h1, p, hr, br, a, div, span, table, img...
* experiment with attributes src, href, width, style...
* experiment with CSS

# HTML 5: Tags

- start (opening) tag :
``` 
<mytag>
``` 
- end (closing) tag:
``` 
</mytag>
``` 
- Tags should be closed
  - in XML-compatible syntax: always
    - in particular with self-closing tags:
    ``` 
    <mytag/>
    ``` 
  - in non-XML syntax: most of the time
    - except for some tags (historical reasons): `img`, `br`, `input`, ...
  - must be closed in the right order
  ``` 
  <a><b></a></b> // wrong
  <a><b></b></a> // correct                                    
  ```
- Tags structure the content of an HTML document into a tree: the [DOM
Tree](#slide-html-dom)

# HTML 5: Attribute

An attribute indicates a property of a DOM element

- specified on the corresponding start tag or self-closing tag
``` 
<mytag property-name='property-value'></mytag>
<mytag property-name='property-value'/>
```
- Using quotes " or single-quotes '
``` 
<mytag name="value"></mytag>
<mytag name='value'/>
```
- Possibly with nested quotes
``` 
<mytag name="value with 'inside'">
<mytag name='value with "inside"'>
```
- Aternate HTML 5 syntaxes (not XML-compatible)
    <mytag name=value> 
    <mytag name> 

# HTML 5: Attributes

Multiple attributes can be specified:

- space separated
```
    <mytag attr1="value1" attr2="value2">
```
- order is not important
```
    <mytag attr2="value2" attr1="value1"> 
```
- cannot duplicate the same attribute twice
```
    <mytag attr1="value2" attr1="value1"> 
```

# HTML 5: A large standard

Defines many tags

- Paragraphs, Tables, Forms
- Multimedia: images, videos, audios
- Graphical Primitives
- …

Defines JavaScript APIs

- Basic document manipulations
- Element-specific APIs (e.g. video)
- Advanced APIs (Offline Storage, Database, communications)
- …

Defines how to integrate with other Web technologies

- Mix of SVG and MathML within the HTML
page

# HTML 5: Demos

- [Demos](http://html5demos.com/){target="_blank"}
- [Mozilla's](https://developer.mozilla.org/en-US/demos/){target="_blank"}


# HTML 5: Tags and Content Model

Tags are organized in different categories

- Root & Metadata: `html`, `head`, `title`, `meta`, `link`, `base`,
`style`
- Scripting: `noscript`, `script`, `template`
- Sections: `body`, `section`, `nav`, `article`, `h1`…`h6`, `header`,
`footer`, `address`, `main`
- Grouping: `div`, `p`, `hr`, `pre`, `blockquote`, `ol`, `ul`, `li`,
`dl`, `dt`, `dd`,`figure`, `figcaption`
- Text: `a`, `em`, `strong`, `small`, `s`, `cite`, `q`, `dfn`, `abbr`,
`data`, `time`, `code`, `var`, `samp`, `kbd`, `sub`, `sup`, `i`,
`b`, `u`, `mark`, `ruby`, `rt`, `rp`, `bdi`, `bdo`, `br`, `wbr`
- Media elements: `img`, `video`, `audio`, `source`, `track`,
`canvas`, `svg`
- Embedded content: `iframe`, `embed`, `object`, `param`, `map`,
`area`, `math`
- Forms: `form`, `fieldset`, `label`, `legend`, `input`, `button`,
`select`, `datalist`, `optgroup`, `option`, `textarea`, `keygen`,
`output`, `progress`, `meter`
- Tables: `table`, `caption`, `tr`, `td`, `th`, `colgroup`, `col`,
`tbody`, `thead`, `tfoot`
- Interactive: `details`, `summary`, `menuitem`, `menu`

Tags can only be nested according to the content model

- Exemple: `td` (table cell) can only be in `tr` (table row)

# HTML 5 Hello World\!

As simple as [that](data:text/html,Hello%20World!){target="_blank"}\!

    Hello World!

Browser's parsing algorithm are very robust (tag soup)

- Will create the page structure for you\!
- Will try to close tags for you\!
- ...

# HTML 5 Basic page structure

```html
<!DOCTYPE html>
<html lang="en">
<head>
<title>This is the title</title>
</head>
<!-- this is a comment -->
<body>
<!-- visible content goes here -->
</body>
</html>
```

# HTML 5 header

- The header of a document is delimited by the `head` tags.
```html
<head> ... </head>
```  
- The header contains meta-informations about the document, such as
its title, encoding, associated files, etc.
- Some common items are:
  - metadata
    - The character set of the page, usually at the very beginning
      of the header (not reliable)        
```html 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="UTF-8">
```
  - The title of the page, displayed in the title bar of Web
    browsers.
```html 
<title>My great website</title>
```
  - Javascript & CSS links

# HTML 5 body

- The content of the document is delimited by the `body` tags.
```html
<body> ... </body>
```  
- The body may be structured into sections, paragraphs, lists, etc.

# HTML 5 body content

- Typically uses tags describe sections, by decreasing order of
importance:
```html
<h1>Title of the page</h1>
<h2>Title of a main section</h2>
<h3>Title of a subsection</h3>
<h4>Title of a subsubsection</h4>
```
- Or paragraphs of text:
```html
<p> ... </p>
```
- Or simple grouping elements without semantics:
```html
<div> ... </div>
```

# Structured body of a page

![](html/image17.png)

# Links

- What differentiates Web pages (hypertext pages) from normal
documents: links\!
- Introduced with `<a> ... </a>`
- Navigating a link can bring to:
  - a resource on another server or another file of the same server        

```html 
<a href="http://www.cnrs.fr/"> 
  <img src="images/cnrs.gif" alt="CNRS">
</a>
<a href="bio/indexbioinfo.html">Bioinformatics</a>                             
```   
  - another part of the same document with anchors

# Relative URLs

- with respect to a context (e.g., the URL of the parent document, the
**base URL**):
- If context is : `https://www.example.com/toto/toto2/toto3`

| relative URL | Absolute URL                       |
| ------------ | ---------------------------------- |
| /titi        | https://www.example.com/titi       |
| tata         | https://www.example.com/toto/toto2/tata       |
| \#tutu       | https://www.example.com/toto/toto2/toto3#tutu |

# Anchors

- Anchors serve to reach a precise point in the document.
- They are defined, either on an existing tag by using the `id`
attribute, or with an 
```
<a name="tutorials">
```
- Then, one can link to this anchor:
```html
<a href="#tutorials">tutorials</a> 
<a href="http://www.w3.org/#tutorials">tutorials</a>              
```

# Lists

  - Unordered lists
    <table>
    <colgroup>
    <col style="width: 70%" />
    <col style="width: 30%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><pre><code>&lt;ul&gt;
    &lt;li&gt;First bullet point&lt;/li&gt;
    &lt;li&gt;Second bullet point&lt;/li&gt;
    &lt;/ul&gt;
                        </code></pre></td>
    <td><ul>
    <li>First bullet point</li>
    <li>Second bullet point</li>
    </ul></td>
    </tr>
    </tbody>
    </table>
  - Ordered lists
    <table>
    <colgroup>
    <col style="width: 70%" />
    <col style="width: 30%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><pre><code>&lt;ol&gt;
    &lt;li&gt;First ordered point&lt;/li&gt;
    &lt;li&gt;Second ordered point&lt;/li&gt;
    &lt;/ol&gt;
                        </code></pre></td>
    <td><ol>
    <li>First ordered point</li>
    <li>Second ordered point</li>
    </ol></td>
    </tr>
    </tbody>
    </table>

# Tables

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><pre><code>                                
&lt;table&gt;
 &lt;tr&gt;
  &lt;td&gt;row 1 - column 1&lt;/td&gt;
  &lt;td&gt;row 1 - column 2&lt;/td&gt;
 &lt;/tr&gt;
 &lt;tr&gt;
  &lt;td&gt;row 2 - column 1&lt;/td&gt;
  &lt;td&gt;row 2 - column 2&lt;/td&gt;
 &lt;/tr&gt;
&lt;/table&gt;
</code></pre></td>
<td><table style="border-collapse: collapse; border: 1px solid black;">
<tbody>
<tr class="odd">
<td style="border: 1px solid black;">row 1 - column 1</td>
<td style="border: 1px solid black;">row 1 - column 2</td>
</tr>
<tr class="even">
<td style="border: 1px solid black;">row 2 - column 1</td>
<td style="border: 1px solid black;">row 2 - column 2</td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

Other options: `th`, `caption`, `thead`, `tbody`, `tfoot`, `col`,
`colgroup`

# Forms

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="vertical-align: top;"><pre><code>&lt;form  action=&quot;demo_form_action.asp&quot; method=&quot;get&quot;&gt;
 &lt;fieldset&gt;
  &lt;legend&gt;Information&lt;/legend&gt;
  First name: &lt;input type=&quot;text&quot;
   name=&quot;firstname&quot;&gt;&lt;br&gt;
  Last name: &lt;input type=&quot;text&quot;
   name=&quot;lastname&quot;&gt;&lt;br&gt;
 &lt;/fieldset&gt;
 Password: &lt;input type=&quot;password&quot;
  name=&quot;pwd&quot;&gt;&lt;br&gt;
 &lt;input type=&quot;radio&quot; name=&quot;sex&quot;
  value=&quot;male&quot;&gt;Male&lt;br&gt;
 &lt;input type=&quot;radio&quot; name=&quot;sex&quot;
  value=&quot;female&quot;&gt;Female&lt;br&gt;
 &lt;input type=&quot;checkbox&quot; name=&quot;vehicle&quot;
  value=&quot;Bike&quot;&gt;I have a bike&lt;br&gt;
 &lt;input type=&quot;checkbox&quot; name=&quot;vehicle&quot; 
  value=&quot;Car&quot;&gt;I have a car&lt;br&gt;
 Date: &lt;input type=&quot;date&quot;
  name=&quot;date&quot;&gt;&lt;br&gt;
 Nationality: &lt;select name=&quot;nationality&quot;&gt;
  &lt;option value=&quot;french&quot;&gt;French&lt;/option&gt;
  &lt;option value=&quot;English&quot;&gt;English&lt;/option&gt;
 &lt;/select&gt;
 &lt;input type=&quot;submit&quot; value=&quot;Send&quot;&gt;
&lt;/form&gt;
</code></pre></td>
<td><form  action="demo_form_action.asp" method="get">
<fieldset style="border: 1px solid black;">
<legend>Information</legend>
First name: <input type="text" name="firstname"><br>
Last name: <input type="text" name="lastname"><br>
</fieldset>
Password: <input type="password" name="pwd"><br>
<input type="radio" name="sex" value="male">Male<br>
<input type="radio" name="sex" value="female">Female<br>
<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
<input type="checkbox" name="vehicle" value="Car">I have a car<br>
Date: <input type="date" name="date"><br>
Nationality<select name="nationality">
  <option value="french">French</option>
  <option value="English">English</option>
</select><br>
<input type="submit" value="Send">
</form>
</td>
</tr>
</tbody>
</table>

Other options: colors, time, ...

# Nested documents

  - Render the content of another page in the current page
  - Using `<iframe>` tags

```html                        
<iframe width="400" height="215" frameborder="0
           scrolling="yes" marginheight="0" marginwidth="0" 
           src="http://www.telecom-paris.fr/...">
</iframe>                                         
```

# Document Object Model

Tree-based representation of an HTML document

DOM Node=

  - DOM Text node
  - DOM Comment node
  - DOM Element
  - DOM Attribute

DOM Nodes, DOM Elements … can be manipulated by script via specific
interfaces

# DOM Tree example

![](html/image18.bmp)

```html 
<html>
 <head>
  <title>My title</title>
 </head>
 <body>
  <a href="http://www.enst.fr/">My link</a>
  <h1>my header</h1>
 </body>
</html>                    
```

Simplified tree: be careful of DOM Text nodes

# Summary of this lesson

* HTML history, HTML5, tags, attributes, JS APIs
* demo, basic structure, basic elements
* DOM = JS API to HTML