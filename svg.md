# Scalable Vector Graphics

![](svg/svg-logo.png)

[pdf](svg.pdf){target="_blank"}

# Vector Graphics

- Contrary to raster/bitmap images (pixel description)
- Graphics are described using mathematical/geometrical primitives
  - 2D objects: lines, curves, circles, rectangles, text
  - or 3D equivalent: meshes, nurbs, spheres, …
- Better suited for simple geometrical shapes, not for natural images,
and when the scene is not complex
- Formats: SVG, VML, AI, PS, PDF, Flash…
- Properties:
  - Can be scaled without pixel artifacts
  - Trade-off image quality vs. rendering cost (Client-side
    rendering vs. server-side rendering)

# Vector Graphics Properties: Zoomability

|   |   |
|---|---|
|![](svg/image34.png)|![](svg/image35.png)|

Source:wikipedia.org

# Vector Graphics Properties: Scalability

|      |                                        |                                        |                                        |          |
| ---- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------- |
|      | ![](svg/image36.png) | ![](svg/image38.png) | ![](svg/image37.png) | Total    |
| PNG  | 25x37 / 1,55 Ko                        | 50x75 / 3,89 Ko                        | 100x150 / 9,89 Ko                      | 15,33 Ko |
| SVG  | \-                                     | any resolution                         | \-                                     | 5,93 Ko  |
| SVGZ | \-                                     | any resolution                         | \-                                     | 1,54 Ko  |


# SVG: a bit of history

- Initial ecosystem:
  - HTML 4.01: 1999
  - CSS 1.0 (2nd ed.): 1996
  - XML 1.0 (2nd ed.): 1998
- Initial competing technologies: VML (Microsoft) and PGML (Adobe)
  - [SVG 1.0](https://www.w3.org/TR/SVG10/){target="_blank"} (2001)
  - [SVG 1.1](http://www.w3.org/TR/2011/REC-SVG11-20110816/){target="_blank"} (2011)
- New ecosystem: tight integration with
  - HTML 5
  - CSS 3
  - [SVG 2](https://svgwg.org/svg2-draft/){target="_blank"} (2016, Candidate
    Recommendation)

# What is SVG ?

- XML standard for
  - 2D Vector Graphics
    - Including text & fonts
    - With specific drawing, layout, positioning rules
  - With support for:
    - Styling (using CSS),
    - Animations (using JavaScript or SMIL),
    - Scripting (using JavaScript)
    - Interactivity (using JavaScript & DOM Events),
    - Raster images (PNG, JPG)
    - Multimedia (audio, video)
- Examples
  - [SVG WoW](http://svg-wow.org/){target="_blank"}
  - [Snap](http://snapsvg.io/demos/){target="_blank"}
  - [SVG animations](http://www.hongkiat.com/blog/svg-animations/){target="_blank"}

# SVG: Benefits/Drawbacks from XML

- Benefit: SVG documents can be handled by generic XML tools
  - Syntax verification, validation
  - Modification using JS/DOM
  - Transformations using XSLT
  - …
- Drawback: Verbose
  - Some attributes are hard to read, such as `d` of `path`
  - XML requires difficult syntax (simplifications in SVG 2)

# SVG Basic Example

```svg
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 600" version="1.1">
 <rect x="100" y="100" width="400" height="200" 
          fill="yellow" stroke="black" stroke-width="3"/>
 <rect x="100" y="350" rx="100" ry="50" width="400" height="200" 
          fill="salmon" stroke="black" stroke-width="3"/>
 <circle cx="100" cy="100" r="80" 
          fill="orange" stroke="navy" stroke-width="10"/>                      
</svg>
```

<div style="width: 35%; border: 1px solid gray;">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 600" version="1.1">
 <rect x="100" y="100" width="400" height="200" 
          fill="yellow" stroke="black" stroke-width="3"/>
 <rect x="100" y="350" rx="100" ry="50" width="400" height="200" 
          fill="salmon" stroke="black" stroke-width="3"/>
 <circle cx="100" cy="100" r="80" 
          fill="orange" stroke="navy" stroke-width="10"/>                      
</svg>
</div>  

# SVG Example with group

```svg
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 600" version="1.1">
 <rect x="100" y="100" width="400" height="200" 
          fill="yellow" stroke="black" stroke-width="3"/>
 <g>
  <rect x="100" y="350" rx="100" ry="50" width="400" height="200" 
          fill="salmon" stroke="black" stroke-width="3"/>
  <circle cx="100" cy="100" r="80" fill="orange" stroke="navy" stroke-width="10"/>
 </g>
</svg>
```

<div style="width: 35%; border: 1px solid gray;">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 600" version="1.1">
 <rect x="100" y="100" width="400" height="200" 
          fill="yellow" stroke="black" stroke-width="3"/>
 <g>
  <rect x="100" y="350" rx="100" ry="50" width="400" height="200" 
          fill="salmon" stroke="black" stroke-width="3"/>
  <circle cx="100" cy="100" r="80" fill="orange" stroke="navy" stroke-width="10"/>
 </g>
</svg>
</div>  

# SVG Files

- Internet Media Type (a.k.a. MIME Type)
  - image/svg+xml
- File Extensions:
  - .svg
  - .svgz when compressed using GZIP

# Basic shapes

Graphical Primitives

`<rect>`

  - Anchored on its top left corner (x, y)
  - Possible rounded corners (rx, ry)

![](svg/image61.png)
![](svg/image62.png)

`<circle>`

`<ellipse>`

![](svg/image63.png)

  - Anchored on its center

# Basic shapes 2

Point/Coordinate-based primitives

  - `<line>`, `<polygon>`, `<polyline>`
  - `<path>` : complex curves

![](svg/image64.png)

# SVG Curves

<img src="svg/image65.png" style="position: absolute; width: 50%; right: 0;"/>

Line segments

Bézier Curves

  - Cubic (C)
  - Cubic Symetrical (S)
  - Quadratic (Q)
  - Quadratic Symetrical (T)

Catmull-Rom Curves (in SVG 2)

  - [Dotty.svg](http://schepers.cc/svg/path/dotty.svg){target="_blank"}

…

# SVG Arcs

  - Start-point, end-point + arc parameter

  
![](svg/image68.png)  
![](svg/image67.png)

# SVG Path

Element used to describe complex graphics

`<path>`

Drawing commands are described using the `d` attribute

  - List of 2D points separated by drawing commands
  - Use of relative or absolute user units

  
![](svg/image69.png)

# Text in SVG

- SVG uses specific elements for text
  - Different from HTML
    - No flowing text
    - No paragraph
  - Graphical primitives as others, can be filled, stroked, …
  - With additional CSS text properties: `font-size`, …
- SVG Text elements
  - `<text>` renders characters on a single line
  - `<tspan>` used to change the style of some characters on a line
  - `<textPath>` draws a text along a path (ex: legend on a river)

![](svg/image70.png)

# Viewing SVG graphics: Vocabulary

![](svg/image27.png)

- SVG canvas
  - Infinite rendering area onto which the graphical elements are
    rendered
- SVG viewbox
  - Rectangular region clipping the canvas defined by the viewbox
    attribute(x y width height)
- SVG window / viewport
  - The viewbox is viewed fitted or sliced to a size width x height
  - Defined by the width, height and preserveAspectRatio attributes
      - Treated as if the viewbox was an image (also used for
        images)

# Positioning SVG: Coordinate Systems

- SVG Canvas Coordinate System
  - X-axis right-wards, Y-axis downward
  - Origin usually corresponding to the top-left corner of the viewbox
- Local Coordinate Systems
  - Origin: typically top-left or center of a shape
- Intermediate Coordinate Systems
  - Transformation of a local coordinate system
  - using `<g>` elements
- Units for positioning and transformations
  - Default arbitrary unit
    - Mapped to physical units based on viewBox
    - Possible to use units from CSS: cm, px, em, …
  - No precision limit

# Example of Local Coordinate Systems

- Graphics
```svg  
<path stroke="black" d="M 100 100 L 200 200"/>
```  

![](svg/image48.png)

- Text
```svg  
<text x="0" y="100" font-size="80" fill="red">Doing text</text>
```  
    
![](svg/image49.png)

# SVG Rendering Model

- Individual graphical element rendering
  - Drawing operations in order
      - Fill then stroke (or stroke then fill), using the paint-order
        attribute
      - Then markers
      - Then filters
      - Then clip
      - Then mask
- Then group rendering (a.k.a. compositing, blending, ...)

![](svg/image71.png)

# Filling Properties

<table class="twocol">
<tr>
<td>

- `fill`
  - A uniform/solid color
    - sRGB color space or ICC color profile: Extensions in SVG 2 Color Module
    - Syntax:
        ```css
        rgb(int[0-255], int[0-255], int[0-255]);
        rgb([0-100]%, [0-100]%, [0-100]%);
        black, white …
        ```
  - A linear or radial gradient
    - Also used in CSS
    - Extensions to Gradient Meshes in SVG 2
  - A pattern
    - Extensions to hatches in SVG 2
- `fill-opacity`
  - Transparency used for alpha-blending
- `fill-rule`
  - When a graphical primitive self-intersects

</td><td style="vertical-align: top; padding-left: 3%;">

![](svg/image72.png)  
![](svg/image74.png)  
![](svg/image73.png)

</td></tr></table>

# Stroking Properties

<table class="twocol">
<tr>
<td>

![](svg/image89.png)  
![](svg/image92.png)
<br>
![](svg/image90.png)
![](svg/image91.png)

</td><td style="vertical-align: top; padding-left: 3%;">

- `stroke`
  - Same syntax/values as `fill` including gradients, pattern, …
- `stroke-opacity`  
  - Same as fill-opacity but only on the stroke
    - Can be combined
- `stroke-width`
  - Centered around the mathematical/geometrical outline
  - New attribute in SVG 2.0 to control the position of the stroke
- `stroke-dasharray` 
- `stroke-dashoffset`
- `stroke-linejoin`
- `stroke-linecap`

</td></tr></table>

# Authoring tools

<img src="svg/image33.png" style="position: absolute; width: 50%; right: 0;"/>

- Your favorite text editor\!
- Commercial tools
  - Adobe: Illustrator, EdgeCode, <br> Edge Animate, Dreamweaver
  - Microsoft: Visio
  - CorelDraw
- Open Source Software
  - [Inkscape](http://inkscape.org){target="_blank"} (GUI editor, <br>Free, Open Source)
  - [SVG Edit](https://svg-edit.github.io/svgedit/releases/svg-edit-2.8.1/svg-editor.html%0A){target="_blank"}
    (editor in the browser)
- SVG Cleaners
  - Inkscape: Save as « Plain » or « Optimized » SVG
  - Scripts for cleaning: scour, …


# Summary of the lesson

* What is vector graphics, history of SVG, pros and cons
* Elements, coordinates, rendering
* Authoring tools